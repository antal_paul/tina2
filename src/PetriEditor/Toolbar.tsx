import * as React from 'react';
import 'core-js';
import '../css/font-awesome.css';
import { Tool, Tools, ToolbarManager } from './Services';
interface ToolbarProps { 
    manager: ToolbarManager; 
}
export class Toolbar extends React.Component<ToolbarProps, void> {
    constructor(props: ToolbarProps) {
        super(props);
        props.manager.addHandler(this.updateView);
    }
    render() {
        const selectTool = this.selectTool;
        return (
            <div>
                <span
                    className={this.getClassName(Tools.Cursor)}
                    onClick={e => selectTool(Tools.Cursor)}>
                    <i className="fa fa-hand-pointer-o" aria-hidden="true"/>
                </span>
                <span
                    className={this.getClassName(Tools.Location)}
                    onClick={e => selectTool(Tools.Location)}>
                    <i className="fa fa-circle-thin" aria-hidden="true"/>
                </span>
                <span
                    className={this.getClassName(Tools.Transition)}
                    onClick={e => selectTool(Tools.Transition)}>
                    <i className="fa fa-square-o" aria-hidden="true"/>
                </span>
                <span
                    className={this.getClassName(Tools.Arc)}
                    onClick={e => selectTool(Tools.Arc)}>
                    <i className="fa fa-long-arrow-right" aria-hidden="true"/>
                </span>
                <span
                    className={this.getClassName(Tools.SimSwitch, [Tools.SimStart])}
                    onClick={e => selectTool(Tools.SimSwitch)}>
                    <i className="fa fa-bolt" aria-hidden="true"/>
                </span>
                <span
                    className={this.getClassName(Tools.SimStart)}
                    onClick={e => selectTool(Tools.SimStart)}>
                    <i className={(this.props.manager.currentTool === Tools.SimStart)
                        ? 'fa fa-stop' : 'fa fa-play'} aria-hidden="true"/>
                </span>
            </div>
        );
    }

    getClassName = (tool: Tool, activeWith: Tool[] = []) => {
        return 'toolButton' + (this.props.manager.currentTool === tool 
        || activeWith.indexOf(this.props.manager.currentTool) !== -1
            ? ' activeTool' : '');
    }

    selectTool = (tool: Tool) => {
        this.props.manager.changeCurrentTool(tool);
    }

    updateView = () => this.forceUpdate();
}