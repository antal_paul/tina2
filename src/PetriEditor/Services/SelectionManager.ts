import { Location, Transition, Arc } from '../PetriShapes';
export type Element = Location | Transition | Arc | undefined;
export class SelectionManager {
    public CurrentElement: Element;
    private OnChangeHandlers: ((oldElement: Element, newElement: Element) => void)[] = [];
    public SetCurrentElement(element: Element) {
        this.OnChangeHandlers.forEach(h => h(this.CurrentElement, element));
        this.CurrentElement = element;
    }

    public AddOnChangeHandler(handler: (oldElement: Element, newElement: Element) => void) {
        this.OnChangeHandlers.push(handler);
    }
    public RemoveOnChangeHandler(handler: (oldElement: Element, newElement: Element) => void) {
        const index = this.OnChangeHandlers.findIndex(h => h === handler);
        if (index > -1) {
            this.OnChangeHandlers.splice(index, 1);
        }
    }
}