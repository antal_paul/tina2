export class IdProvider {
    private static currentId = 1;
    public static getNewId() {
        return (IdProvider.currentId++) + IdProvider.getGuid();
    }

    private static getGuid() {
        return 'xxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c: string) {
            // tslint:disable-next-line:no-bitwise
            var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}