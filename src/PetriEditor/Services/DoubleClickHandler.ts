import { fabric } from 'fabric';
const time = 200;
export class DoubleClickHandler {
    private doubleClickTriggered: boolean = false;
    private newClickTime: number;
    private lastClickTime: number;
    private newPointer: { x: number, y: number };
    private lastPointer: { x: number, y: number };
    private canvas: fabric.Canvas;

    private onClick: (e: fabric.IEvent) => void;
    private onDoubleClick: (e: fabric.IEvent) => void;

    constructor(
        canvas: fabric.Canvas,
        onClick: (e: fabric.IEvent) => void,
        onDoubleClick: (e: fabric.IEvent) => void
    ) {
        this.canvas = canvas;
        this.onClick = onClick;
        this.onDoubleClick = onDoubleClick;
    }

    public onMouseDown = (event: fabric.IEvent) => {
        this.lastClickTime = this.newClickTime;
        this.lastPointer = this.newPointer;
        this.newPointer = this.canvas.getPointer(event.e);
        this.newClickTime = +new Date();

        setTimeout(this.timeoutCallback, time, event);

    }

    private timeoutCallback = (e: fabric.IEvent) => {
        if (this.doubleClickTriggered) {
            this.doubleClickTriggered = false;
        } else if (this.isDoubleClick()) {
            this.doubleClickTriggered = true;
            this.onDoubleClick(e);
        } else {
            this.onClick(e);
        }
    }

    private isDoubleClick() {
        return this.newClickTime - this.lastClickTime < time &&
            this.lastPointer.x === this.newPointer.x &&
            this.lastPointer.y === this.newPointer.y;
    }
}