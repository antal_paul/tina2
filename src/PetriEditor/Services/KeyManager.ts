export class KeyManager {
    private static CurrentKeys: KeyboardEvent[] = [];
    private static handlers: ((keys: KeyboardEvent[]) => void)[] = [];
    public static ArePressed(keys: string[]): boolean {
        return !keys.find(k =>
            KeyManager.CurrentKeys.findIndex(ck =>
                ck.key.toUpperCase() === k.toUpperCase()) === -1);
    }

    public static AddHandler(handler: (keys: KeyboardEvent[]) => void) {
        KeyManager.handlers.push(handler);
    }
    public static RemoveHandler(handler: (k: KeyboardEvent[]) => void) {
        const index = KeyManager.handlers.indexOf(handler);
        if (index > -1) {
            KeyManager.handlers.splice(index, 1);
        }
    }
    public static AddKey(k: KeyboardEvent) {
        const index = KeyManager.CurrentKeys.findIndex(x => x.key.toUpperCase() === k.key.toUpperCase());
        if (index >= 0) {
            return;
        }
        KeyManager.CurrentKeys.push(k);
        KeyManager.handlers.forEach((h) => h(KeyManager.CurrentKeys));
    }

    public static RemoveKey(k: KeyboardEvent) {
        const index = KeyManager.CurrentKeys.findIndex(x => x.key.toUpperCase() === k.key.toUpperCase());
        if (index < 0) {
            return;
        }
        KeyManager.CurrentKeys.splice(index, 1);
    }

    static initialize() {
        window.onkeydown = (e: KeyboardEvent) => { KeyManager.onKeyDown(e); };
        window.onkeyup = (e: KeyboardEvent) => { KeyManager.onKeyUp(e); };
    }

    private static onKeyDown(e: KeyboardEvent) {
        KeyManager.AddKey(e);
    }

    private static onKeyUp(e: KeyboardEvent) {
        KeyManager.RemoveKey(e);
    }
}

KeyManager.initialize();