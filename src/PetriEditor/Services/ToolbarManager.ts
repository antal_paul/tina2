export type Tool = 'pointer' | 'location' | 'transition' | 'arc'
    | 'simSwitch' | 'simStart';
export class Tools {
    public static Cursor: Tool = 'pointer';
    public static Location: Tool = 'location';
    public static Transition: Tool = 'transition';
    public static Arc: Tool = 'arc';
    public static SimSwitch: Tool = 'simSwitch';
    public static SimStart: Tool = 'simStart';
}

type handler = ((newTool: Tool) => void);

export class ToolbarManager {
    public currentTool: Tool = 'pointer';
    private handlers: handler[] = [];
    public changeCurrentTool(newTool: Tool) {
        if (newTool === this.currentTool) {
            if (newTool === Tools.SimStart) {
                this.currentTool = Tools.SimSwitch;
                this.handlers.forEach(h => h(this.currentTool));
            }
            return;
        }
        this.currentTool = newTool;
        this.handlers.forEach(h => h(this.currentTool));
    }

    public addHandler(handler: handler) {
        const index = this.handlers.indexOf(handler);
        if (index !== -1) {
            return;
        }
        this.handlers.push(handler);
    }

    public removeHandler(handler: handler) {
        const index = this.handlers.indexOf(handler);
        if (index === -1) {
            return;
        }
        this.handlers.splice(index, 1);
    }
}