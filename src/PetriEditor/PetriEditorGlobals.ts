import { fabric } from 'fabric';
import { SelectionManager, ToolbarManager } from './Services';
import { SerializedPetri } from './Petri';
import { PetriSimState } from './PetriSimState';

export class PetriEditorGlobals {
    public canvas: fabric.Canvas;
    public selectionManager: SelectionManager;
    public toolbarManager: ToolbarManager;
    public simState: PetriSimState | null;
    public inSimulationMode: boolean;
    public isNameValid: (name: string, currentId: string) => boolean;
    private eventHandlers: (() => void)[] = [];
    public changeMode(mode?: 'edit' | 'sim', state?: SerializedPetri) {
        if (!mode) {
            mode = this.inSimulationMode ? 'edit' : 'sim';
        }
        if (mode === 'edit') {
            this.inSimulationMode = false;
            if (this.simState) {
                this.simState.pauseRandomSim();
            }
            this.simState = null;
        } else if (mode === 'sim' && state) {
            this.inSimulationMode = true;
            this.simState = new PetriSimState(state);
        }
        this.eventHandlers.forEach(h => h());
    }
    public addModeChangeHandler(handler: () => void) {
        const index = this.eventHandlers.findIndex(h => h === handler);
        if (index !== -1) {
            return;
        }
        this.eventHandlers.push(handler);
    }
    public removeModeChangeHandler(handler: () => void) {
        const index = this.eventHandlers.findIndex(h => h === handler);
        if (index === -1) {
            return;
        }
        this.eventHandlers.splice(index, 1);
    }

    public clearAllHandlers() {
        this.eventHandlers = [];
    }
    constructor(
        canvas: fabric.Canvas,
        selectionManager: SelectionManager,
        toolbarManager: ToolbarManager,
        inSimulationMode: boolean,
        isNameValid: (name: string, currentId: string) => boolean
    ) {
        this.canvas = canvas;
        this.selectionManager = selectionManager;
        this.toolbarManager = toolbarManager;
        this.inSimulationMode = inSimulationMode;
        this.isNameValid = isNameValid;
    }
};