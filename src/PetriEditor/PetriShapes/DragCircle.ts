import { fabric } from 'fabric';

export class DragCircle extends fabric.Circle {
    private static circleOptions: fabric.ICircleOptions = { 
        radius: 5, 
        fill: 'white',
        originX: 'center', 
        originY: 'center', 
        stroke: 'black', 
        strokeWidth: 2, 
        lockRotation: true,
        hasBorders: false,
        hasControls: false,
        hasRotatingPoint: false,
    };
    private arcHandlers: { 
        segment: fabric.Line, 
        moveHandler: ((e: fabric.IEvent) => void), 
        removeHandler: () => void 
    }[] = [];

    constructor(x: number, y: number) {
        super({...DragCircle.circleOptions, top: y, left: x});
        this.on('moving', (e: fabric.IEvent) => {
            if (this.arcHandlers) {
                this.arcHandlers.forEach(h => h.moveHandler(e));
            }
        });
    }

    public addArc(segment: fabric.Line,
                  moveHandler: (e: fabric.IEvent) => void,
                  removeHandler: () => void
    ) {
        this.arcHandlers.push({ segment, moveHandler, removeHandler });
    }

    public removeArc(segment: fabric.Line) {
        const index = this.arcHandlers.findIndex(x => x.segment === segment);
        this.arcHandlers.splice(index, 1);
    }

    public getX() {
        return this.left || 0;
    }
    public getY() {
        return this.top || 0;
    }
}