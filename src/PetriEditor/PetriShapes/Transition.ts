import { fabric } from 'fabric';
import { PetriEditorGlobals } from '../PetriEditorGlobals';
import { IdProvider } from '../Services';
const squareLength = 40;

interface TransitionProps {
    x: number;
    y: number;
    id: string;

    transitionText: fabric.IText;
    transitionRect: fabric.Rect;

    group: fabric.Group;

    arcHandlers: { segment: fabric.Line, moveHandler: ((e: fabric.IEvent) => void), removeHandler: () => void }[];

    name: string;
    isPopped: boolean;

    onChange: () => void;
}

export class SerializedTransition {
    x: number;
    y: number;
    id: string;
    name: string;

    constructor(x: number, y: number, name: string, id: string) {
        this.x = x;
        this.y = y;
        this.name = name;
        this.id = id;
    }
}

export class Transition {
    private static counter: number = 0;

    private static rectOptions: fabric.IRectOptions = {
        fill: 'white',
        top: 20,
        stroke: 'black',
        strokeWidth: 3,
        originX: 'center',
        width: squareLength,
        height: squareLength,
        lockRotation: true,
        hasBorders: false,
        hasControls: false,
        hasRotatingPoint: false,
    };

    private static nameOptions: fabric.IITextOptions = {
        fontSize: 15,
        fontFamily: 'Arial',
        fontWeight: 800,
        originX: 'center',
        textAlign: 'center',
        editable: true,
        lockRotation: true,
        hasControls: false,
        hasRotatingPoint: false,
    };

    private static groupOptions: fabric.IObjectOptions = {
        originX: 'center',
        originY: 'center',

        lockRotation: true,
        hasBorders: false,
        hasControls: false,
        hasRotatingPoint: false
    };

    public props: TransitionProps;
    private lastClickTime: number | null;
    private newClickTime: number;
    private globals: PetriEditorGlobals;

    public static deserialize(
        ser: SerializedTransition,
        onChange: () => void,
        globals: PetriEditorGlobals
    ) {
        return new Transition(
            onChange,
            ser.x,
            ser.y,
            globals,
            ser.id,
            ser.name,
        );
    }

    public serialize(): SerializedTransition {
        return new SerializedTransition(this.props.x, this.props.y, this.props.name, this.props.id);
    }

    constructor(
        onChange: () => void,
        x: number, y: number,
        globals: PetriEditorGlobals,
        id?: string,
        name?: string,
    ) {
        if (!id) {
            id = IdProvider.getNewId();
        }
        if (!name) {
            name = 't' + Transition.counter++;
        }
        this.globals = globals;
        this.props = {
            ...this.props,
            x, y, onChange, name, id, arcHandlers: [],
            isPopped: false
        };
        this.init();
    }

    public removeFromCanvas() {
        const handlers = [...this.props.arcHandlers];
        handlers.forEach(h => h.removeHandler());
        this.globals.canvas.remove(this.props.group);
    }

    public addArc(
        segment: fabric.Line,
        moveHandler: (e: fabric.IEvent) => void,
        removeHandler: () => void
    ) {
        this.props.arcHandlers.push({ segment, moveHandler, removeHandler });
    }

    public removeArc(segment: fabric.Line) {
        const index = this.props.arcHandlers.findIndex(x => x.segment === segment);
        this.props.arcHandlers.splice(index, 1);
    }

    public getX() {
        return this.props.group.left || 0;
    }
    public getY() {
        return (this.props.group.top || 0) + 7;
    }

    private pop(popOut: boolean) {
        const props = this.props;
        if (props.isPopped === popOut) {
            return;
        }
        props.isPopped = popOut;
        const newScale = popOut ? 1.2 : 1;
        props.group.scale(newScale);
    }

    private init() {
        const props = this.props;
        props.transitionText = new fabric.IText(props.name, Transition.nameOptions);
        this.setName(props.name);
        props.transitionText.on('editing:exited', (e: fabric.IEvent) => this.recreateGroup());
        props.transitionRect = new fabric.Rect(Transition.rectOptions);
        props.group = this.createGroup(props.transitionText, props.transitionRect);
        this.globals.canvas.add(props.group);
        this.globals.addModeChangeHandler(this.updateMode);
    }

    private updateMode = () => {
        if (this.globals.inSimulationMode) {
            if (this.globals.simState) {
                const tSimState = this.globals.simState.TransitionSimStates.get(this.props.id);
                if (tSimState) {
                    this.updateColor(tSimState.isSatisfied);
                    tSimState.addIsSatisfiedChangedHandler(this.updateColor);
                }
            }
        } else {
            this.setFillColor('white');
        }
    }

    private updateColor = (isSatisfied: boolean) => {
        const color = isSatisfied ? 'green' : 'red';
        this.setFillColor(color);
    }

    private setFillColor(color: 'red' | 'green' | 'white') {
        this.props.transitionRect.set('fill', color);
        this.props.onChange();
    }

    private createGroup(text: fabric.IText, transitionRect: fabric.Rect) {
        const props = this.props;
        text.setTop(Transition.nameOptions.top || 0);
        transitionRect.setTop(Transition.rectOptions.top || 0);
        text.setLeft(Transition.nameOptions.left || 0);
        transitionRect.setLeft(Transition.rectOptions.left || 0);
        const group = new fabric.Group([text, transitionRect], Transition.groupOptions);
        group.left = props.x;
        group.top = props.y;
        group.on('selected', (e: fabric.IEvent) => {
            this.pop(true);
            this.globals.canvas.bringToFront(this.props.group);
            this.globals.selectionManager.SetCurrentElement(this);
        });
        group.on('deselected', (e: fabric.IEvent) => {
            this.pop(false);
            // if (!KeyManager.ArePressed(['Alt'])) {
            //     this.globals.selectionManager.SetCurrentElement(undefined);
            // }
        });
        group.on('mouseup', this.onMouseUp);
        group.on('moving', (e: fabric.IEvent) => {
            if (this.props.arcHandlers) {
                this.props.arcHandlers.forEach(h => h.moveHandler(e));
            }
            this.props.x = group.left || 0;
            this.props.y = group.top || 0;
        });
        return group;
    }

    private onDoubleClick(e: fabric.IEvent) {
        if (!this.globals.inSimulationMode) {
            const canvas = this.globals.canvas;
            this.props.x = this.props.group.left || 0;
            this.props.y = this.props.group.top || 0;
            this.pop(true);
            this.props.group.destroy();
            canvas.remove(this.props.group);
            this.props.transitionRect.selectable = false;
            canvas.add(this.props.transitionRect);
            canvas.add(this.props.transitionText);
            canvas.setActiveObject(this.props.transitionText);
            this.props.transitionText.enterEditing();
        }
    }

    private onMouseUp = (e: fabric.IEvent) => {
        this.newClickTime = +new Date();
        if (this.lastClickTime && this.newClickTime - this.lastClickTime < 300) {
            this.lastClickTime = null;
            this.onDoubleClick(e);
        }
        this.lastClickTime = this.newClickTime;

        if (this.globals.inSimulationMode && this.globals.simState) {
            const simState = this.globals.simState.TransitionSimStates.get(this.props.id);
            if (simState && simState.isSatisfied) {
                simState.activate();
            }
        }
    }

    private recreateGroup = () => {
        this.props.transitionRect.scale(1);
        this.props.transitionText.scale(1);
        this.props.group = this.createGroup(this.props.transitionText, this.props.transitionRect);
        this.globals.canvas.add(this.props.group);
        this.globals.canvas.setActiveObject(this.props.group);
        this.props.isPopped = true;
        this.pop(false);
        this.setName(this.props.transitionText.text || '');
        this.globals.canvas.remove(this.props.transitionText);
        this.globals.canvas.remove(this.props.transitionRect);
    }

    private setName(name: string) {
        const newLineIndex = name.indexOf('\n');
        if (name.length === 0) {
            name = this.props.name;
        }
        if (newLineIndex !== -1) {
            name = name.substr(0, newLineIndex);
        }
        while (!this.globals.isNameValid(name, this.props.id)) {
            name = '*' + name;
        }
        this.props.name = name;
        this.props.transitionText.setText(this.props.name);
        this.props.onChange();
    }
}