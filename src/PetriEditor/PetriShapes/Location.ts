import { fabric } from 'fabric';
import { PetriEditorGlobals } from '../PetriEditorGlobals';
import { IdProvider } from '../Services';
const circleRadius = 20;

interface LocationProps {
    x: number;
    y: number;
    id: string;

    locText: fabric.IText;
    locGroup: fabric.Group;
    locValue: fabric.IText;
    group: fabric.Group;

    arcHandlers: { segment: fabric.Line, moveHandler: ((e: fabric.IEvent) => void), removeHandler: () => void }[];

    name: string;
    value: number;
    isPopped: boolean;

    onChange: () => void;
}

export class SerializedLocation {
    x: number;
    y: number;
    id: string;
    name: string;
    value: number;

    constructor(x: number, y: number, id: string, name: string, value: number) {
        this.x = x;
        this.y = y;
        this.name = name;
        this.value = value;
        this.id = id;
    }
}

export class Location {

    private static counter: number = 0;

    private static circleOptions: fabric.ICircleOptions = {
        fill: 'white',
        top: 20,
        stroke: 'black',
        strokeWidth: 3,
        originX: 'center',
        radius: circleRadius,
        lockRotation: true,
        hasBorders: false,
        hasControls: false,
        hasRotatingPoint: false,
    };

    private static nameOptions: fabric.IITextOptions = {
        fontSize: 15,
        fontFamily: 'Arial',
        fontWeight: 800,
        originX: 'center',
        textAlign: 'center',
        editable: true,
        lockRotation: true,
        hasControls: false,
        hasRotatingPoint: false,
    };

    private static valueOptions: fabric.IITextOptions = {
        fontSize: 15,
        fontFamily: 'Arial',
        fontWeight: 'normal',
        fill: 'black',
        top: circleRadius + 20,
        textAlign: 'center',
        originX: 'center',
        originY: 'center',
        lockRotation: true,
        hasControls: false,
        hasRotatingPoint: false,
    };

    private static groupOptions: fabric.IObjectOptions = {
        originX: 'center',
        originY: 'center',

        lockRotation: true,
        hasBorders: false,
        hasControls: false,
        hasRotatingPoint: false
    };

    private static locOptions: fabric.IObjectOptions = {
        top: 20,
        stroke: 'black',
        originX: 'center',
        lockRotation: true,
        hasBorders: false,
        hasControls: false,
        hasRotatingPoint: false,
    };

    public props: LocationProps;
    private lastClickTime: number | null;
    private newClickTime: number;
    private globals: PetriEditorGlobals;

    public static deserialize(
        ser: SerializedLocation,
        onChange: () => void,
        globals: PetriEditorGlobals
    ) {
        return new Location(
            onChange,
            ser.x,
            ser.y,
            globals,
            ser.value,
            ser.id,
            ser.name,
        );
    }

    public serialize(): SerializedLocation {
        return new SerializedLocation(
            this.props.x,
            this.props.y,
            this.props.id,
            this.props.name,
            this.props.value
        );
    }

    constructor(
        onChange: () => void,
        x: number,
        y: number,
        globals: PetriEditorGlobals,
        value?: number,
        id?: string,
        name?: string,
    ) {
        if (!id) {
            id = IdProvider.getNewId();
        }
        if (!name) {
            name = 'p' + Location.counter++;
        }
        this.globals = globals;
        this.props = {
            ...this.props,
            x, y, onChange, id, name, arcHandlers: [], value: value || 0,
            isPopped: false
        };
        this.init();
    }

    public removeFromCanvas() {
        const handlers = [...this.props.arcHandlers];
        handlers.forEach(h => h.removeHandler());
        this.globals.canvas.remove(this.props.group);
    }

    public addArc(
        segment: fabric.Line,
        moveHandler: (e: fabric.IEvent) => void,
        removeHandler: () => void
    ) {
        this.props.arcHandlers.push({ segment, moveHandler, removeHandler });
    }

    public removeArc(segment: fabric.Line) {
        const index = this.props.arcHandlers.findIndex(x => x.segment === segment);
        this.props.arcHandlers.splice(index, 1);
    }

    public getX() {
        return this.props.group.left || 0;
    }
    public getY() {
        return (this.props.group.top || 0) + 7;
    }

    private pop(popOut: boolean) {
        const props = this.props;
        if (props.isPopped === popOut) {
            return;
        }
        props.isPopped = popOut;
        const newScale = popOut ? 1.2 : 1;
        props.group.scale(newScale);
    }

    private init() {
        const props = this.props;
        props.locText = new fabric.IText(props.name, Location.nameOptions);
        props.locText.on('editing:exited', (e: fabric.IEvent) => this.recreateGroup());
        this.setName(props.name);
        const circle = new fabric.Circle(Location.circleOptions);
        props.locValue = new fabric.IText(
            this.props.value > 0 ? this.props.value + '' : '',
            Location.valueOptions);
        const locGroup = new fabric.Group([circle, props.locValue], Location.locOptions);
        props.locGroup = locGroup;
        props.group = this.createGroup(props.locText, props.locGroup);
        this.globals.canvas.add(props.group);
        this.globals.addModeChangeHandler(this.updateMode);
    }

    private updateMode = () => {
        if (this.globals.inSimulationMode && this.globals.simState) {
            const simState = this.globals.simState.LocationSimStates.get(this.props.id);
            if (simState) {
                simState.addValueChangedHandler(this.setValue);
            }
        }
    }

    private createGroup(text: fabric.IText, locGroup: fabric.Group) {
        const props = this.props;
        text.setTop(Location.nameOptions.top || 0);
        locGroup.setTop(Location.locOptions.top || 0);
        text.setLeft(Location.nameOptions.left || 0);
        locGroup.setLeft(Location.locOptions.left || 0);
        const group = new fabric.Group([text, locGroup], Location.groupOptions);
        group.left = props.x;
        group.top = props.y;
        group.on('selected', (e: fabric.IEvent) => {
            this.pop(true);
            this.globals.canvas.bringToFront(this.props.group);
            this.globals.selectionManager.SetCurrentElement(this);
        });
        group.on('deselected', (e: fabric.IEvent) => {
            this.pop(false);
            // if (!KeyManager.ArePressed(['Alt'])) {
            //     this.globals.selectionManager.SetCurrentElement(undefined);
            // }
        });
        group.on('mouseup', this.onMouseUp);
        group.on('mousewheel', (e: fabric.IEvent) => {
            if (!this.globals.inSimulationMode) {
                // tslint:disable-next-line:no-any
                const wheelEvent: any = e.e;
                const value = props.value + wheelEvent.deltaY / -100;
                this.setValue(value);
            }
        });
        group.on('moving', (e: fabric.IEvent) => {
            if (this.props.arcHandlers) {
                this.props.arcHandlers.forEach(h => h.moveHandler(e));
            }
            this.props.x = group.left || 0;
            this.props.y = group.top || 0;
        });
        return group;
    }

    private setValue = (newVal: number) => {
        if (newVal < 0) {
            newVal = 0;
        }
        const props = this.props;
        props.value = newVal;
        props.locValue.set({ text: props.value > 0 ? props.value + '' : '' });
        props.onChange();
    }

    private onDoubleClick(e: fabric.IEvent) {
        if (!this.globals.inSimulationMode) {
            const canvas = this.globals.canvas;
            this.props.x = this.props.group.left || 0;
            this.props.y = this.props.group.top || 0;
            this.pop(true);
            this.props.group.destroy();
            canvas.remove(this.props.group);

            this.props.locGroup.selectable = false;
            canvas.add(this.props.locGroup);
            canvas.add(this.props.locText);
            canvas.setActiveObject(this.props.locText);
            this.props.locText.enterEditing();
        }
    }

    private onMouseUp = (e: fabric.IEvent) => {
        this.newClickTime = +new Date();
        if (this.lastClickTime && this.newClickTime - this.lastClickTime < 300) {
            this.lastClickTime = null;
            this.onDoubleClick(e);
        }
        this.lastClickTime = this.newClickTime;
    }

    private recreateGroup = () => {
        this.props.locText.scale(1);
        this.props.locGroup.scale(1);
        this.props.group = this.createGroup(this.props.locText, this.props.locGroup);
        this.props.isPopped = true;
        this.globals.canvas.add(this.props.group);
        this.pop(false);
        this.setName(this.props.locText.text || '');
        this.globals.canvas.remove(this.props.locText);
        this.globals.canvas.remove(this.props.locGroup);
    }

    private setName(name: string) {
        const newLineIndex = name.indexOf('\n');
        if (name.length === 0) {
            name = this.props.name;
        }
        if (newLineIndex !== -1) {
            name = name.substr(0, newLineIndex);
        }
        while (!this.globals.isNameValid(name, this.props.id)) {
            name = '*' + name;
        }
        this.props.name = name;
        this.props.locText.setText(this.props.name);
    }
}