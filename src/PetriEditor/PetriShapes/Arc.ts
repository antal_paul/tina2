import { fabric } from 'fabric';
import { Location, Transition, DragCircle } from '../PetriShapes';
import { DoubleClickHandler } from '../Services';
import { PetriEditorGlobals } from '../PetriEditorGlobals';

type Node = Location | Transition | DragCircle;
type Segment = {
    line: fabric.Line,
    start: Node,
    end: Node,
    head?: fabric.Triangle,
};
interface ArcProps {
    start: Location | Transition;
    end: Location | Transition;
    segments: Segment[];

    group: fabric.Group;
    valueText: fabric.IText;

    value: number;

    onChange: () => void;
    onDelete: (arc: Arc) => void;
}

export class SerializedArc {
    startId: string;
    endId: string;
    intermediaryPoints: { x: number, y: number }[];
    value: number;
    
    constructor(
        startId: string,
        endId: string,
        intermediaryPoints: { x: number, y: number }[],
        value: number) {
        this.startId = startId;
        this.endId = endId;
        this.intermediaryPoints = intermediaryPoints;
        this.value = value;
    }
}

const headDistance = 30;

export class Arc {
    private static lineOptions: fabric.IObjectOptions = {
        strokeWidth: 2,
        stroke: 'black',
        perPixelTargetFind: true,
        padding: 10,
        lockMovementX: true,
        lockMovementY: true,
        hoverCursor: 'pointer',
        lockRotation: true,
        hasBorders: false,
        hasControls: false,
        hasRotatingPoint: false,
    };

    private static triangleOptions: fabric.ITriangleOptions = {
        width: 15,
        height: 15,
        stroke: 'black',
        strokeWidth: 2,
        fill: 'white',
        originX: 'center',
        originY: 'top',
        selectable: false
    };

    private static valueOptions: fabric.IITextOptions = {
        fontSize: 15,
        fontFamily: 'Arial',
        fontWeight: 'normal',
        textAlign: 'center',
        originX: 'center',
        originY: 'center',
        lockRotation: true,
        hasControls: false,
        hasRotatingPoint: false,
        editable: false
    };

    public props: ArcProps;
    private globals: PetriEditorGlobals;

    public static deserialize(
        ser: SerializedArc,
        getNodeById: (id: string) => Location | Transition | undefined,
        globals: PetriEditorGlobals,
        onChange: () => void,
        onDelete: (a: Arc) => void
    ) {
        const start = getNodeById(ser.startId);
        const end = getNodeById(ser.endId);
        if (!start || !end) {
            // tslint:disable-next-line:no-console
            console.error(ser);
            throw Error('Error deserializing arc from: ');
        }
        return new Arc(
            start,
            end,
            ser.value,
            onChange,
            onDelete,
            globals,
            ser.intermediaryPoints);
    }

    private static moveHead(start: Node, end: Node, head: fabric.Triangle, text: fabric.IText) {
        const dx = end.getX() - start.getX();
        const dy = end.getY() - start.getY();
        const angle = Math.atan2(dy, dx) * (180 / Math.PI) + 90;
        const distance = Math.sqrt(Math.pow(end.getX() - start.getX(), 2)
            + Math.pow(end.getY() - start.getY(), 2));
        const x = end.getX() - ((headDistance * (end.getX() - start.getX())) / distance);
        const y = end.getY() - ((headDistance * (end.getY() - start.getY())) / distance);
        head.set({
            top: y,
            left: x,
            angle: angle,
        });
        head.setCoords();
        let textX, textY;
        if (start.getX() > end.getX()) {
            textX = x + 20;
        } else {
            textX = x - 20;
        }
        if (start.getY() > end.getY()) {
            textY = y + 20;
        } else {
            textY = y - 20;
        }

        text.set({ top: textY, left: textX });
        text.setCoords();
    }

    public serialize(): SerializedArc {
        const sortedSegments = this.getSortedSegments();
        if (!sortedSegments) {
            throw new Error('error reading arc');
        }
        const intermediaryPoints = sortedSegments.map(p => ({ x: p.end.getX(), y: p.end.getY() }));
        intermediaryPoints.splice(intermediaryPoints.length - 1, 1);
        return new SerializedArc(
            this.props.start.props.id,
            this.props.end.props.id,
            intermediaryPoints,
            this.props.value);
    }

    constructor(
        start: Location | Transition,
        end: Location | Transition,
        value: number,
        onChange: () => void,
        onDelete: (arc: Arc) => void,
        globals: PetriEditorGlobals,
        intermediaryPoints?: { x: number, y: number }[]
    ) {
        if (value < 1) {
            value = 1;
        }
        this.globals = globals;
        const valueText = new fabric.IText(value > 1 ? value + '' : '', Arc.valueOptions);
        globals.canvas.add(valueText);
        this.props = {
            ...this.props,
            start, end, segments: [], intermediaryPoints: [], value, valueText, onChange, onDelete
        };
        if (!intermediaryPoints) {
            intermediaryPoints = [];
        }
        const circles = intermediaryPoints.map(p => new DragCircle(p.x, p.y));
        circles.forEach(c => globals.canvas.add(c));
        const points = [start, ...circles, end];
        for (let i = 0; i < points.length - 1; i++) {
            this.makeSegment(points[i], points[i + 1]);
        }
    }

    public removeFromCanvas = () => {
        this.props.segments.forEach(s => {
            s.start.removeArc(s.line);
            s.end.removeArc(s.line);
            this.globals.canvas.remove(s.line);
            if (s.head) {
                this.globals.canvas.remove(s.head);
            }
            if (s.start instanceof DragCircle) {
                this.globals.canvas.remove(s.start);
            }
            if (s.end instanceof DragCircle) {
                this.globals.canvas.remove(s.end);
            }
        });
        this.globals.canvas.remove(this.props.valueText);
        this.props.onDelete(this);
    }

    private makeSegment(start: Node, end: Node) {
        const line = new fabric.Line(
            [start.getX(),
            start.getY(),
            end.getX(),
            end.getY()],
            Arc.lineOptions);
        const segment: Segment = { line: line, start, end };
        if (end === this.props.end) {
            const head = new fabric.Triangle(Arc.triangleOptions);
            Arc.moveHead(start, end, head, this.props.valueText);
            this.globals.canvas.sendToBack(head);
            segment.head = head;
        }
        this.globals.canvas.sendToBack(line);

        line.setCoords();
        start.addArc(
            segment.line,
            (e) => {
                segment.line.set('x1', start.getX() || 0);
                segment.line.set('y1', start.getY() || 0);
                segment.line.setCoords();
                if (segment.head) {
                    Arc.moveHead(segment.start, segment.end, segment.head, this.props.valueText);
                }
            },
            this.removeFromCanvas);
        end.addArc(
            line,
            (e) => {
                segment.line.set('x2', end.getX() || 0);
                segment.line.set('y2', end.getY() || 0);
                segment.line.setCoords();
                if (segment.head) {
                    Arc.moveHead(segment.start, segment.end, segment.head, this.props.valueText);
                }
            },
            this.removeFromCanvas);
        const handler = new DoubleClickHandler(
            this.globals.canvas,
            // tslint:disable-next-line:no-empty
            (e) => { },
            this.onDoubleClick
        );
        segment.line.on('selected', (e) => {
            this.globals.selectionManager.SetCurrentElement(this);
            this.highlight();
        });
        segment.line.on('deselected', (e) => {
            this.globals.selectionManager.SetCurrentElement(undefined);
            this.highlight(false);
        });
        segment.line.on('mousedown', handler.onMouseDown);
        segment.line.on('mousewheel', (e: fabric.IEvent) => {
            if (!this.globals.inSimulationMode) {
                // tslint:disable-next-line:no-any
                const wheelEvent: any = e.e;
                this.props.value += wheelEvent.deltaY / -100;
                if (this.props.value < 1) {
                    this.props.value = 1;
                }
                this.props.valueText.set({ text: this.props.value > 1 ? this.props.value + '' : '' });
                this.props.onChange();
            }
        });
        this.props.segments.push(segment);
    }

    private onDoubleClick = (e: fabric.IEvent) => {
        // tslint:disable-next-line:whitespace
        const segment = <fabric.Line>e.target;
        const index = this.props.segments.findIndex(x => x.line === segment);
        const pointer = this.globals.canvas.getPointer(e.e);
        const el = this.props.segments[index];
        if (index > -1) {
            el.start.removeArc(el.line);
            el.end.removeArc(el.line);
            this.props.segments.splice(index, 1);
            const x = pointer.x;
            const y = pointer.y;
            const circle = new DragCircle(x, y);
            circle.on('selected', (ev) => {
                this.globals.selectionManager.SetCurrentElement(this);
                this.highlight();
            });
            circle.on('deselected', (ev) => {
                this.globals.selectionManager.SetCurrentElement(undefined);
                this.highlight(false);
            });
            this.globals.canvas.add(circle);
            this.makeSegment(el.start, circle);
            this.makeSegment(circle, el.end);
            this.globals.canvas.setActiveObject(circle);
            this.globals.canvas.remove(segment);
            if (el.head) {
                this.globals.canvas.remove(el.head);
            }
        }
    }

    private highlight(on: boolean = true) {
        const color = on ? 'orange' : 'black';
        this.props.segments.forEach(s => {
            s.line.set('stroke', color);
            if (s.head) {
                s.head.set('stroke', color);
            }
            if (s.start instanceof DragCircle) {
                s.start.set('stroke', color);
            }
            if (s.end instanceof DragCircle) {
                s.end.set('stroke', color);
            }
        });

    }
    private getSortedSegments() {
        const first = this.props.segments.find(s => s.start === this.props.start);
        if (!first) {
            return;
        }
        const segments = [first];
        let current = first;
        do {
            const next = this.props.segments.find(s => s.start === current.end);
            if (!next) {
                break;
            }
            current = next;
            segments.push(current);
        } while (true);
        return segments;
    }
}