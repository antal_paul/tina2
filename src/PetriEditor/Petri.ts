import { fabric } from 'fabric';
import { PetriEditorGlobals } from './PetriEditorGlobals';
import {
    Location,
    SerializedLocation,
    Transition,
    SerializedTransition,
    Arc,
    SerializedArc
} from './PetriShapes';
import { SelectionManager, KeyManager, Element, ToolbarManager, Tools, Tool } from './Services';

fabric.Group.prototype.hasControls = false;

export class SerializedPetri {
    locs: SerializedLocation[];
    trans: SerializedTransition[];
    arcs: SerializedArc[];
}

export class Petri {
    private savedCanvas: SerializedPetri;
    private savedNetworkForSimulation: SerializedPetri;
    private globals: PetriEditorGlobals;
    private entities: (Location | Transition)[] = [];
    private arcs: Arc[] = [];
    private borderHolder: HTMLDivElement;

    public serialize() {
        const result = new SerializedPetri();
        const serializedLocations = this.entities.filter(e => e instanceof Location)
            .map(e => (<Location> e).serialize());
        const serializedTransitions = this.entities.filter(e => e instanceof Transition)
            .map(e => (<Transition> e).serialize());
        const serializedArcs = this.arcs.map(a => a.serialize());
        result.locs = serializedLocations;
        result.trans = serializedTransitions;
        result.arcs = serializedArcs;
        return result;
    }

    public deserialize(petri: SerializedPetri) {
        this.clearScreen();
        const locs = petri.locs.map(e => Location.deserialize(e, this.updateCanvas, this.globals));
        const trans = petri.trans.map(e => Transition.deserialize(e, this.updateCanvas, this.globals));
        this.entities = [...locs, ...trans];
        const getById = (id: string) => this.entities.find(e => e.props.id === id);
        this.arcs = petri.arcs.map(a => Arc.deserialize(
            a,
            getById,
            this.globals,
            this.updateCanvas,
            this.deleteArc
        ));
    }

    constructor(
        canvasElement: HTMLCanvasElement | string,
        borderHolder: HTMLDivElement,
        toolbarManager: ToolbarManager
    ) {
        const selectionManager = new SelectionManager();
        const canvas = new fabric.Canvas(canvasElement, {
            targetFindTolerance: 10,
            preserveObjectStacking: true,
            selection: false
        });
        this.borderHolder = borderHolder;
        canvas.setWidth(1920);
        canvas.setHeight(1080);

        this.globals = new PetriEditorGlobals(
            canvas,
            selectionManager,
            toolbarManager,
            false,
            this.isNameValid,
        );
        this.registerHandlers();

    }

    private registerHandlers() {
        this.globals.canvas.on('mouse:up', this.onMouseUp);
        this.globals.selectionManager.AddOnChangeHandler(this.onChangeSelection);
        KeyManager.AddHandler(this.onDelete);
        KeyManager.AddHandler(this.onSaveLoad);
        KeyManager.AddHandler(this.onNew);
        KeyManager.AddHandler(this.onSwitchMode);
        this.globals.toolbarManager.addHandler(this.onToolSelect);
    }

    private onMouseUp = (e: fabric.IEvent) => {
        if (!e.target) {
            const pointer = this.globals.canvas.getPointer(e.e);
            if (!this.globals.inSimulationMode && !this.createNode(pointer)) {
                this.globals.selectionManager.SetCurrentElement(undefined);
            }
        }
    }

    private isNameValid = (name: string, id: string) => {
        const entity = this.entities.find(e => e.props.name === name && e.props.id !== id);
        return entity === undefined;
    }

    private createNode(pointer: { x: number, y: number }) {
        const currentTool = this.globals.toolbarManager.currentTool;
        if (currentTool === Tools.Location ||
            currentTool === Tools.Cursor && KeyManager.ArePressed(['Control'])) {
            this.entities.push(new Location(
                this.updateCanvas,
                pointer.x,
                pointer.y,
                this.globals
            ));
            return true;
        } else if (currentTool === Tools.Transition ||
            currentTool === Tools.Cursor && KeyManager.ArePressed(['Shift'])) {
            this.entities.push(new Transition(
                this.updateCanvas,
                pointer.x,
                pointer.y,
                this.globals
            ));
            return true;
        }
        return false;
    }

    private onChangeSelection = (oldElement: Element, newElement: Element) => {
        if (!oldElement
            || !newElement
            || oldElement instanceof Arc
            || newElement instanceof Arc
            || oldElement instanceof Location === newElement instanceof Location) {
            return;
        }
        if (!this.globals.inSimulationMode) {
            const currentTool = this.globals.toolbarManager.currentTool;
            if (currentTool === Tools.Arc ||
                currentTool === Tools.Cursor && KeyManager.ArePressed(['Alt'])
                && this.arcs.findIndex(a =>
                    a.props.start === oldElement
                    && a.props.end === newElement) === -1) {
                this.arcs.push(new Arc(
                    oldElement,
                    newElement,
                    1,
                    this.updateCanvas,
                    this.deleteArc,
                    this.globals)
                );
            }
        }
    }

    private onNew = (keys: KeyboardEvent[]) => {
        if (KeyManager.ArePressed(['Alt', 'Control', 'N'])
        ) {
            this.clearScreen();
        }
    }

    private onSwitchMode = (keys: KeyboardEvent[]) => {
        if (KeyManager.ArePressed(['Alt', 'Control', 'D'])
        ) {
            this.switchSimState();

        }
    }

    private onToolSelect = (newTool: Tool) => {
        if (newTool === Tools.SimSwitch) {
            this.switchSimState(true);
            if (this.globals.simState) {
                this.globals.simState.pauseRandomSim();
            }
        } else if (newTool === Tools.SimStart) {
            this.switchSimState(true);
            if (this.globals.simState) {
                this.globals.simState.startRandomSim();
            }
        } else {
            this.switchSimState(false);
        }
    }

    private switchSimState(switchOn?: boolean) {
        if (switchOn === undefined) {
            switchOn = !this.globals.inSimulationMode;
        }
        if (this.globals.inSimulationMode && !switchOn) {
            this.globals.clearAllHandlers();
            this.deserialize(this.savedNetworkForSimulation);
            this.borderHolder.classList.add('canvasEditMode');
            this.borderHolder.classList.remove('canvasSimulationMode');
            this.globals.changeMode('edit');
        } else if (!this.globals.inSimulationMode && switchOn) {
            this.savedNetworkForSimulation = this.serialize();
            this.borderHolder.classList.add('canvasSimulationMode');
            this.borderHolder.classList.remove('canvasEditMode');
            this.globals.canvas.discardActiveObject();
            this.globals.changeMode('sim', this.savedNetworkForSimulation);
        }
    }

    private clearScreen() {
        this.arcs.forEach(a => a.removeFromCanvas());
        this.entities.forEach(e => e.removeFromCanvas());
        this.entities = [];
    }

    private onSaveLoad = (keys: KeyboardEvent[]) => {
        if (this.globals.inSimulationMode && this.globals.simState) {
            this.globals.simState.toggleRandomSim();
            return;
        }
        if (KeyManager.ArePressed(['Alt', 'Control', 'S'])) {
            this.savedCanvas = this.serialize();
        } else if (KeyManager.ArePressed(['Alt', 'Control', 'L'])) {
            this.deserialize(this.savedCanvas);
        }
    }
    private deleteArc = (arc: Arc) => {
        const index = this.arcs.findIndex(a => a === arc);
        if (index > -1) {
            this.arcs.splice(index, 1);
        }
    }

    private onDelete = (keys: KeyboardEvent[]) => {
        if (this.globals.inSimulationMode) {
            return;
        }
        const node = this.globals.selectionManager.CurrentElement;
        if (
            node
            && (node instanceof Location || node instanceof Transition || node instanceof Arc)
            && keys.find(k => k.key === 'Delete')
        ) {
            node.removeFromCanvas();
            if (this.globals.selectionManager.CurrentElement === node) {
                this.globals.selectionManager.SetCurrentElement(undefined);
            }
            if (node instanceof Arc) {
                const index = this.arcs.indexOf(node);
                this.arcs.splice(index, 1);
            } else {
                const index = this.entities.indexOf(node);
                this.entities.splice(index, 1);
            }
        }
    }

    private updateCanvas = () => {
        if (this.globals && this.globals.canvas) {
            this.globals.canvas.renderAll();
        }
    }
}