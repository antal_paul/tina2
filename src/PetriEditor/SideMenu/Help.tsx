import * as React from 'react';
import 'core-js';

export class Help extends React.Component<{}, void> {
    render() {
        return (
            <div>
                <h2> Drawing controls: </h2>
                <p><b><em>Ctrl + click</em></b> - create location</p>
                <p><b><em>Shift + click</em></b> - create transition</p>
                <p>
                    <b><em>Alt + click</em></b>
                    - create arc between currently selected element and the clicked element
                </p>
                <p><b><em>Delete</em></b> - delete currently selected element</p>
                <p><b><em>Double click</em></b> - to edit names (Esc to exit input) or to add corners to arcs</p>
                <p><b><em>Mouse wheel</em></b> - increase / decrease values of locations / arcs</p>
                <hr />
                <h2> Editor controls: </h2>
                <p><b><em>Ctrl + Alt + N</em></b> - clear screen (new)</p>
                <p><b><em>Ctrl + Alt + S</em></b> - put in cache (save)</p>
                <p><b><em>Ctrl + Alt + L</em></b> - get from cache (load)</p>
                <p>
                    <b><em>Ctrl + Alt + D</em></b>{'\u00A0'}
                     - switch between edit and simulation mode ( green - edit | red - simulation)
                </p>
            </div >
        );
    }
}