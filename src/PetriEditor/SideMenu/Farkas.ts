export class Farkas {
    public static transposeMatrix(matrix: number[][]) {
        const result: number[][] = [];
        for (let i = 0; i < matrix[0].length; i++) {
            result[i] = [];
            for (let j = 0; j < matrix.length; j++) {
                result[i][j] = matrix[j][i];
            }
        }
        return result;
    }
    public static calculate(system: number[][]) {
        const matrix = [];
        for (let i = 0; i < system.length; i++) {
            matrix.push([...system[i]]);
        }
        const m = matrix[0].length;
        for (let i = 0; i < matrix.length; i++) {
            for (let j = 0; j < matrix.length; j++) {
                if (j === i) {
                    matrix[i].push(1);
                } else {
                    matrix[i].push(0);
                }
            }
        }

        for (let col = 0; col < m; col++) {
            for (let i = 0; i < matrix.length; i++) {
                for (let j = i + 1; j < matrix.length; j++) {
                    if (matrix[i][col] * matrix[j][col] < 0) {
                        this.addRows(matrix, j, i, Math.abs(matrix[i][col]), Math.abs(matrix[j][col]));
                    }
                }
            }
            for (let i = matrix.length - 1; i >= 0; i--) {
                if (matrix[i][col] !== 0) {
                    matrix.splice(i, 1);
                }
            }
        }
        for (let i = 0; i < matrix.length; i++) {
            matrix[i].splice(0, m);
        }
        return matrix;
    }

    private static addRows(
        matrix: number[][],
        destRow: number,
        sourceRow: number,
        destCoef: number,
        sourceCoef: number
    ) {
        const newRow = [];
        for (var i = 0; i < matrix[0].length; i++) {
            newRow[i] = matrix[destRow][i] * destCoef
                + matrix[sourceRow][i] * sourceCoef;
        }
        matrix.push(newRow);
    }
}