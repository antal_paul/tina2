import { SerializedPetri } from '../Petri';
import { PetriNetModel } from './PetriNetModel';

interface Edge {
    startMarking: number[];
    endMarking: number[];
    transitionId: string;
    transitionName: string;
}

export interface Graph {
    locationNames: string[];
    initalMarking: number[];
    markings: number[][];
    edges: Edge[];
    isBounded: boolean;
    isReversible: boolean | undefined;
    isPseudoViable: boolean;
}

export class PetriReachAnalyser {
    public static getReachabilityGraph(ser: SerializedPetri) {
        const model = PetriNetModel.getFromSerialized(ser);
        const M0 = ser.locs.map(l => l.value);
        return PetriReachAnalyser.getGraph(model, M0);
    }
    private static getGraph(model: PetriNetModel, M0: number[]) {
        const locationNames = model.locs.map(l => l.name);
        const graph: Graph = {
            locationNames,
            edges: [],
            markings: [M0],
            initalMarking: M0,
            isBounded: false,
            isReversible: false,
            isPseudoViable: false
        };
        const work = [M0];
        while (work.length > 0) {
            const startMarking = work.pop();
            if (!startMarking) {
                break;
            }
            const enabledTrans = model.getEnabledTransitions(startMarking);
            for (let i = 0; i < enabledTrans.length; i++) {
                const nextMark = model.applyTransition(enabledTrans[i].id, startMarking);
                if (!nextMark) {
                    throw new Error('Tried aplying a disabled transition');
                }
                const newMark = this.addOmegas(graph, startMarking, nextMark);
                const existingMark = graph.markings.find(m => this.compareMarkings(m, newMark) === 0);
                if (!existingMark) {
                    graph.markings.push(newMark);
                    work.push(newMark);
                }
                const endMarking = existingMark || newMark;
                graph.edges.push({
                    startMarking,
                    endMarking,
                    transitionId: enabledTrans[i].id,
                    transitionName: enabledTrans[i].name
                });
            }
        }
        graph.isBounded = this.isBounded(graph);
        graph.isReversible = this.isReversible(graph);
        const transitionids = model.trans.map(t => t.id);
        graph.isPseudoViable = this.isPseudoViable(graph, transitionids);
        return graph;
    }

    private static addOmegas(graph: Graph, mark: number[], nextMark: number[]) {
        const loopedMarks = graph.markings.filter(m => this.compareMarkings(m, nextMark) === -1
            && this.chainExists(m, mark, graph.edges)
        );
        if (this.compareMarkings(mark, nextMark) === -1) {
            loopedMarks.push(mark);
        }
        loopedMarks.forEach(m => {
            for (let i = 0; i < nextMark.length; i++) {
                if (this.compareMarkValue(nextMark[i], m[i]) === 1) {
                    nextMark[i] = -1;
                }
            }
        });
        return nextMark;
    }

    private static compareMarkings(mark1: number[], mark2: number[]) {
        if (mark1.length !== mark2.length) {
            throw new Error('Tried to compare two markings of different sizes');
        }
        let sign = 0;
        for (let i = 0; i < mark1.length; i++) {
            const comp = this.compareMarkValue(mark1[i], mark2[i]);
            if (sign === 0 && comp !== 0) {
                sign = mark1[i] < mark2[i] ? -1 : 1;
            } else if (comp !== 0 && sign !== comp) {
                return;
            }
        }
        return sign;
    }

    private static chainExists(startMark: number[], endMark: number[], existingEdges: Edge[]) {
        const visited = existingEdges.map(e => false);
        const edges = existingEdges.map((e, i) => ({index: i, edge: e}));
        const queue = [startMark];
        while (queue.length > 0) {
            const current = queue[0];
            queue.splice(0, 1);
            const currentEdges = edges
                .filter((e) =>
                    !visited[e.index]
                    && this.compareMarkings(current, e.edge.startMarking) === 0
                );
            for (let i = 0; i < currentEdges.length; i++) {
                if (this.compareMarkings(endMark, currentEdges[i].edge.endMarking) === 0) {
                    return true;
                }
                visited[currentEdges[i].index] = true;
                queue.push(currentEdges[i].edge.endMarking);
            }
        }
        return false;
    }

    private static compareMarkValue(a: number, b: number) {
        if (a === b) {
            return 0;
        }
        if (a === -1 && b !== -1) {
            return 1;
        }
        if (a !== -1 && b === -1) {
            return -1;
        }
        return a < b ? -1 : 1;
    }

    private static isBounded(graph: Graph) {
        return graph.markings.findIndex(m => m.findIndex(v => v === -1) !== -1) === -1;
    }

    private static isReversible(graph: Graph) {
        return this.isBounded(graph) ? graph.markings.findIndex(m =>
            !this.chainExists(m, graph.initalMarking, graph.edges)) === -1 : undefined;
    }

    private static isPseudoViable(graph: Graph, transitionIds: string[]) {
        return transitionIds.findIndex(id => graph.edges.findIndex(e => e.transitionId === id) === -1) !== -1;
    }
}