import { SerializedPetri } from '../Petri';

class ArcModel {
    public node: LocationModel;
    public value: number;

    constructor(node: LocationModel, value: number) {
        this.node = node;
        this.value = value;
    }
}

class LocationModel {
    public id: string;
    public name: string;
    public index: number;
    constructor(id: string, name: string, index: number) {
        this.id = id;
        this.name = name;
        this.index = index;
    }
}

class TransitionModel {
    public id: string;
    public name: string;
    private ingoingArcs: ArcModel[] = [];
    private outgoingArcs: ArcModel[] = [];

    constructor(id: string, name: string) {
        this.id = id;
        this.name = name;
    }

    public addIngoingArc(loc: LocationModel, value: number) {
        this.ingoingArcs.push(new ArcModel(loc, value));
    }

    public addOutgoingArc(loc: LocationModel, value: number) {
        this.outgoingArcs.push(new ArcModel(loc, value));
    }

    public apply(marking: number[]) {
        const newMarking = [...marking];
        if (!this.enabled(newMarking)) {
            return;
        }
        this.ingoingArcs.forEach(a => {
            if (newMarking[a.node.index] !== -1) { newMarking[a.node.index] -= a.value; }
        });
        this.outgoingArcs.forEach(a => {
            if (newMarking[a.node.index] !== -1) { newMarking[a.node.index] += a.value; }
        });
        return newMarking;
    }

    public enabled(marking: number[]) {
        return this.ingoingArcs.findIndex(a => marking[a.node.index] !== -1
            && marking[a.node.index] < a.value) === -1;
    }
}
export class PetriNetModel {
    public locs: LocationModel[] = [];
    public trans: TransitionModel[] = [];

    public static getFromSerialized(ser: SerializedPetri) {
        const model = new PetriNetModel();
        ser.locs.forEach((e, i) => model.locs.push(new LocationModel(e.id, e.name, i)));
        ser.trans.forEach(e => model.trans.push(new TransitionModel(e.id, e.name)));

        ser.arcs.forEach(a => {
            const start = model.locs.find(l => l.id === a.startId) || model.trans.find(t => t.id === a.startId);
            const end = model.locs.find(l => l.id === a.endId) || model.trans.find(t => t.id === a.endId);
            if (!start || !end) {
                throw new Error('Error during deserialization');
            }
            if (start instanceof LocationModel && end instanceof TransitionModel) {
                end.addIngoingArc(start, a.value);
            } else if (start instanceof TransitionModel && end instanceof LocationModel) {
                start.addOutgoingArc(end, a.value);
            } else {
                throw new Error('Error during deserialization');
            }
        });
        return model;
    }

    public applyTransition(id: string, marking: number[]) {
        const tran = this.trans.find(t => t.id === id);
        if (!tran || !tran.enabled(marking)) {
            return;
        }
        return tran.apply(marking);
    }

    public getEnabledTransitions(marking: number[]) {
        return this.trans.filter(t => t.enabled(marking));
    }
}