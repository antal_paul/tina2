import * as React from 'react';
import { SerializedPetri } from '../Petri';
import { PetriReachAnalyser, Graph } from './PetriReachAnalyser';
import * as vis from 'vis';
import { Popup } from '../../Popup';
import '../../css/font-awesome.css';

type visData = {
    nodes: vis.DataSet<{
        id: number;
        label: string;
        color: string;
        border: string;
    }>;
    edges: vis.DataSet<{
        from: number;
        to: number;
        label: string;
        arrows: string;
        font: {
            align: string;
        };
    }>;
} | undefined;

interface ReachabilityProps {
    getState: () => SerializedPetri;
}

interface ReachabilityState {
    reachabilityGraph: string;
    visGraph: visData;
    isPopupOpen: boolean;
}

const options = {
    autoResize: true,
    height: '100%',
    width: '100%'
};
export class Reachability extends React.Component<ReachabilityProps, ReachabilityState> {
    private canvas: HTMLDivElement;

    constructor() {
        super();
        this.state = { reachabilityGraph: '', visGraph: undefined, isPopupOpen: false };
    }

    render() {
        if (this.state.visGraph) {
            new vis.Network(this.canvas, this.state.visGraph, options);
        }
        return (
            <div>
                <h3>Reachability Graph</h3>
                <div>
                    <textarea className="reachText" value={this.state.reachabilityGraph} />
                </div>
                <button onClick={() => this.calculate()}>Calculate</button>
                {this.state.visGraph ? <button onClick= {this.openPopup}>View graph</button> : null}
                <hr />
                <Popup isOpen={this.state.isPopupOpen}>
                    <div className="reachPopup">
                        <i
                            className="reachPopup-close fa  fa-times"
                            aria-hidden="true"
                            onClick={this.closePopup}
                        />
                        <div
                            className="reachCanvas"
                            ref={this.onContainerMounted}
                        />
                    </div>
                </Popup>
            </div>

        );
    }

    private onContainerMounted = (canvas: HTMLDivElement) => {
        this.canvas = canvas;
    }

    private drawGraph = (graph: Graph) => {
        const nodes = new vis.DataSet(graph.markings.map((m, i) => {
            var node = {
                id: i,
                label: 'State ' + i + '\n' +
                this.getLocationNames(graph.locationNames, m),
                color: 'rgba(97,195,238,0.5)',
                border: 'rgba(97,195,238,0.8)',
            };
            if (i === graph.markings.indexOf(graph.initalMarking)) {
                node.color = 'lightgray';
                node.border = 'gray';
            }
            return node;
        }
        ));

        const edges = new vis.DataSet(graph.edges.map((e) => (
            {
                from: graph.markings.indexOf(e.startMarking),
                to: graph.markings.indexOf(e.endMarking),
                label: e.transitionName,
                arrows: 'to',
                font: { align: 'middle' }
            }
        )));

        return { nodes: nodes, edges: edges };
    }

    private openPopup = () => this.setState({ isPopupOpen: true });
    private closePopup = () => this.setState({ isPopupOpen: false });

    private calculate() {
        const state = this.props.getState();
        const reachability = PetriReachAnalyser.getReachabilityGraph(state);
        const visGraph = this.drawGraph(reachability);
        const reachabilityGraph = this.getGraphString(reachability);
        this.setState({ ...this.state, reachabilityGraph, visGraph });
    }

    private getGraphString(graph: Graph) {
        let result = '';
        result += 'Bounded: ' + graph.isBounded
            + '\nReversible: ' + (graph.isReversible === undefined ? 'unknown' : graph.isReversible)
            + '\nPseudo-viable: ' + graph.isPseudoViable
            + '\n\n';
        graph.markings.forEach((m, i) => {
            result += 'State ' + i + '\n';
            result += 'props\n';
            result += this.getLocationNames(graph.locationNames, m);
            result += '\ntrans\n';
            graph.edges
                .filter(e => e.startMarking === m)
                .forEach(e => {
                    result += e.transitionName
                        + ' -> '
                        + graph.markings.indexOf(e.endMarking)
                        + ', ';
                });
            result += '\n\n---------------------\n\n';
        });
        return result;
    }

    private getLocationNames = (locationNames: string[], marking: number[]) => {
        let result = '';
        locationNames.forEach((name, j) => {
            if (marking[j] === 0) {
                return;
            } else if (marking[j] === -1) {
                result += name + '*' + 'ω ';
            } else {
                result += name + '*' + marking[j] + ' ';
            }
        });
        return result;
    }
}