import * as React from 'react';
import { SerializedPetri } from '../Petri';

declare const onSave: (json: string, name: string) => void;
declare const networkName: string;

interface SaveLoadProps {
    getState: () => SerializedPetri;
    loadState: (s: SerializedPetri) => void;
}

interface SaveLoadState {
    networkName: string;
}

export class SaveLoad extends React.Component<SaveLoadProps, SaveLoadState> {
    constructor() {
        super();
        const netName = typeof networkName === 'string' ? networkName : '';
        this.state = { networkName: netName };
    }
    render() {
        return (
            <div>
                <h3> Save to My Petri Nets </h3>
                <div>
                    <input
                        value={this.state.networkName}
                        placeholder="Network name"
                        onChange={e => this.onNameChanged(e)}
                    />
                    <button onClick={() => this.save()}>Save</button>
                </div>
                <hr/>
                <h3> Import / Export </h3>
                <button onClick={() => this.export()}>Export</button>
                <button onClick={() => this.import()}>Import</button>
            </div>
        );
    }

    onNameChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ networkName: e.target.value });
    }

    save() {
        if (typeof onSave === 'undefined') {
            return;
        }
        const json = JSON.stringify(this.props.getState());
        onSave(json, this.state.networkName);
    }

    export() {
        const fileContents = JSON.stringify(this.props.getState());
        const filename = 'network.json';
        const filetype = 'text/json';

        const a = document.createElement('a');
        const dataURI = 'data:' + filetype +
            ';base64,' + btoa(fileContents);
        a.href = dataURI;
        a.download = filename;
        var e = document.createEvent('MouseEvents');
        // Use of deprecated function to satisfy TypeScript.
        e.initMouseEvent(
            'click', true, false,
            document.defaultView, 0, 0, 0, 0, 0,
            false, false, false, false, 0, null);
        a.dispatchEvent(e);
        a.remove();
    }

    import() {
        const input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.click();
        input.addEventListener('change', (e) => this.loadOnCanvas(e));
    }

    loadOnCanvas(e: Event) {
        const reader = new FileReader();
        reader.onloadend = file => {
            const text = reader.result;
            const ser: SerializedPetri = JSON.parse(text);
            this.props.loadState(ser);
        };
        // tslint:disable-next-line:no-any
        const target: any = e.target;
        reader.readAsText(target.files[0]);
    }
}