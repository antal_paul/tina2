import * as React from 'react';
import { SerializedPetri } from '../Petri';
import { SerializedLocation, SerializedTransition } from '../PetriShapes';
import { Farkas } from './Farkas';
import 'core-js';

interface StructuralProps {
    getState: () => SerializedPetri;
}

interface StructuralState {
    pInvar: string;
    tInvar: string;
}

export class Structural extends React.Component<StructuralProps, StructuralState> {
    constructor(props: StructuralProps) {
        super(props);
        this.state = { pInvar: '', tInvar: '' };
    }
    render() {
        return (
            <div>
                <h3> Structural analysis </h3>
                <div>
                    <button onClick={() => this.calculate()}>
                        Calculate Invariants
                    </button>
                </div>
                <hr/>
                <h4> P - Invariants </h4>
                <div>
                    <textarea className="structText" value={this.state.pInvar} />
                </div>
                <hr/>
                
                <h4> T - Invariants </h4>
                <div>
                    <textarea className="structText" value={this.state.tInvar} />
                </div>
            </div>
        );
    }

    private calculate() {
        const pResult = this.calculatePInvariants();
        const tResult = this.calculateTInvariants();
        const pInvar = this.getMatrixString(pResult.invariants, pResult.names);
        const tInvar = this.getMatrixString(tResult.invariants, tResult.names);

        this.setState({ ...this.state, pInvar, tInvar });
    }

    private calculatePInvariants() {
        const model = this.getModel();
        const invariants = Farkas.calculate(model.matrix);
        const names = model.locs.map(l => l.name);
        return { invariants, names };
    }

    private calculateTInvariants() {
        const model = this.getModel();
        const matrix = Farkas.transposeMatrix(model.matrix);
        const invariants = Farkas.calculate(matrix);
        const names = model.trans.map(t => t.name);
        return { invariants, names };
    }

    private getModel() {
        const state = this.props.getState();
        const locs = state.locs;
        const trans = state.trans;
        const matrix: number[][] = [];
        for (let i = 0; i < locs.length; i++) {
            matrix[i] = [];
            for (let j = 0; j < trans.length; j++) {
                matrix[i].push(0);
            }
        }
        for (let i = 0; i < state.arcs.length; i++) {
            const arc = state.arcs[i];
            const start = locs.find(l => l.id === arc.startId) || trans.find(t => t.id === arc.startId);
            const end = locs.find(l => l.id === arc.endId) || trans.find(t => t.id === arc.endId);
            if (
                !start
                || !end
                || ((start instanceof SerializedLocation)
                    === (end instanceof SerializedLocation))
            ) {
                throw new Error('Error deserializing arc: invalid ids');
            }
            const coef = start instanceof SerializedLocation ? -1 : 1;
            const { loc, tran } = start instanceof SerializedLocation
                && end instanceof SerializedTransition
                ? { loc: start, tran: end } : { loc: end, tran: start };
            const locIndex = locs.findIndex(l => l === loc) || 0;
            const tranIndex = trans.findIndex(t => t === tran) || 0;
            matrix[locIndex][tranIndex] = arc.value * coef;
        }
        return { locs, trans, matrix };
    }

    private getMatrixString(matrix: number[][], names: string[]) {
        let result = '';
        for (let i = 0; i < matrix.length; i++) {
            for (let j = 0; j < matrix[0].length; j++) {
                if (matrix[i][j]) {
                    result += '  ' + names[j];
                    if (matrix[i][j] > 1) {
                        result += '*' + matrix[i][j];
                    }
                }
            }
            result += '\n';
        }
        return result;
    }
}