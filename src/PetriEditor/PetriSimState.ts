import { SerializedPetri } from './Petri';
import { SerializedLocation, SerializedTransition, SerializedArc } from './PetriShapes';

export class ArcSimState {
    public value: number;
    public node: LocationSimState | TransitionSimState;

    constructor(value: number, node: LocationSimState | TransitionSimState) {
        this.value = value;
        this.node = node;
    }
}

export class LocationSimState {
    public id: string;
    public value: number;
    private outgoingArcs: ArcSimState[] = [];
    private valueChangedHandlers: ((newVal: number) => void)[] = [];

    constructor(id: string, value: number) {
        this.id = id;
        this.value = value;
    }

    public addValueChangedHandler(handler: (newVal: number) => void) {
        const index = this.valueChangedHandlers.findIndex(h => h === handler);
        if (index !== -1) {
            return;
        }
        this.valueChangedHandlers.push(handler);
    }
    public removeValueChangedHandler(handler: (newVal: number) => void) {
        const index = this.valueChangedHandlers.findIndex(h => h === handler);
        if (index === -1) {
            return;
        }
        this.valueChangedHandlers.splice(index, 1);
    }

    public addOutgoingArc(value: number, target: TransitionSimState) {
        const index = this.outgoingArcs.findIndex(a => a.node.id === target.id);
        if (index !== -1) {
            return;
        }
        this.outgoingArcs.push(new ArcSimState(value, target));
    }

    public setValue(newVal: number) {
        if (newVal === this.value) {
            return;
        }
        this.value = newVal;
        this.onValueChange();
    }
    private onValueChange() {
        this.outgoingArcs.forEach(a => {
            if (a.node instanceof TransitionSimState) {
                a.node.updateState();
            }
        });
        this.valueChangedHandlers.forEach(h => h(this.value));
    }
}

export class TransitionSimState {
    public id: string;
    public isSatisfied: boolean = true;

    private isSatisfiedChangedHandlers: ((newState: boolean) => void)[] = [];
    private ingoingArcs: ArcSimState[] = [];
    private outgoingArcs: ArcSimState[] = [];

    constructor(id: string) {
        this.id = id;
    }

    public addIngoingArc(value: number, target: LocationSimState) {
        const index = this.ingoingArcs.findIndex(a => a.node.id === target.id);
        if (index !== -1) {
            return;
        }
        this.ingoingArcs.push(new ArcSimState(value, target));
        this.updateState();
    }

    public addIsSatisfiedChangedHandler(handler: (newState: boolean) => void) {
        const index = this.isSatisfiedChangedHandlers.findIndex(h => h === handler);
        if (index !== -1) {
            return;
        }
        this.isSatisfiedChangedHandlers.push(handler);
    }
    public removeIsSatisfiedChangedHandler(handler: (newState: boolean) => void) {
        const index = this.isSatisfiedChangedHandlers.findIndex(h => h === handler);
        if (index === -1) {
            return;
        }
        this.isSatisfiedChangedHandlers.splice(index, 1);
    }
    public addOutgoingArc(value: number, target: LocationSimState) {
        const index = this.outgoingArcs.findIndex(a => a.node.id === target.id);
        if (index !== -1) {
            return;
        }
        this.outgoingArcs.push(new ArcSimState(value, target));
    }

    public activate() {
        this.updateState();
        if (!this.isSatisfied) {
            return;
        }
        this.ingoingArcs.forEach(a => {
            if (!(a.node instanceof LocationSimState) || a.value > a.node.value) {
                return;
            }
            a.node.setValue(a.node.value - a.value);
        });

        this.outgoingArcs.forEach(a => {
            if (!(a.node instanceof LocationSimState)) {
                return;
            }
            a.node.setValue(a.node.value + a.value);
        });

        this.updateState();
    }

    public updateState() {
        const index = this.ingoingArcs.findIndex(a => {
            if (!(a.node instanceof LocationSimState)) {
                return false;
            }
            return a.node.value < a.value;
        });
        if (this.isSatisfied === (index === -1)) {
            return;
        }
        this.setIsStatisfied(index === -1);
    }

    private setIsStatisfied(newState: boolean) {
        this.isSatisfied = newState;
        this.isSatisfiedChangedHandlers.forEach(h => h(this.isSatisfied));
    }
}

export class PetriSimState {

    public LocationSimStates: Map<string, LocationSimState>;
    public TransitionSimStates: Map<string, TransitionSimState>;
    private interval: number | undefined;

    constructor(state: SerializedPetri) {
        const locations = state.locs.map(e => <SerializedLocation> e);
        const transitions = state.trans.map(e => <SerializedTransition> e);

        this.LocationSimStates = new Map<string, LocationSimState>();
        locations.forEach(this.addLocation);

        this.TransitionSimStates = new Map<string, TransitionSimState>();
        transitions.forEach(this.addTransition);

        state.arcs.forEach(this.addArc);

    }

    public runRandomStep = () => {
        const trans = Array.from(this.TransitionSimStates.values())
            .filter(t => t.isSatisfied);
        if (trans.length > 0) {
            trans[Math.floor(Math.random() * trans.length)].activate();
        }
    }

    public startRandomSim = () => {
        this.interval = window.setInterval(this.runRandomStep, 500);
    }

    public pauseRandomSim = () => {
        if (this.interval) {
            window.clearInterval(this.interval);
            this.interval = undefined;
        }
    }

    public toggleRandomSim() {
        if (this.interval) {
            this.pauseRandomSim();
        } else {
            this.startRandomSim();
        }
    }

    private addLocation = (l: SerializedLocation) => {
        const simState = new LocationSimState(l.id, l.value);
        this.LocationSimStates.set(l.id, simState);
    }

    private addTransition = (t: SerializedTransition) => {
        const simState = new TransitionSimState(t.id);
        this.TransitionSimStates.set(t.id, simState);
    }

    private addArc = (a: SerializedArc) => {
        if (this.LocationSimStates.has(a.startId)
            && this.TransitionSimStates.has(a.endId)) {
            const start = this.LocationSimStates.get(a.startId);
            const end = this.TransitionSimStates.get(a.endId);
            if (!start || !end) {
                return;
            }
            start.addOutgoingArc(a.value, end);
            end.addIngoingArc(a.value, start);
        } else if (this.TransitionSimStates.has(a.startId)
            && this.LocationSimStates.has(a.endId)) {
            const start = this.TransitionSimStates.get(a.startId);
            const end = this.LocationSimStates.get(a.endId);
            if (!start || !end) {
                return;
            }
            start.addOutgoingArc(a.value, end);
        }
    }
}
