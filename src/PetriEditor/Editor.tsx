import * as React from 'react';
import { Petri, SerializedPetri } from './Petri';
import { SideMenu } from './SideMenu';
import { Toolbar } from './Toolbar';
import { ToolbarManager } from './Services';

declare const networkJson: string;

export class Editor extends React.Component<{ id: string, className: string }, null> {
    private petriEditor: Petri;
    private toolbarManager: ToolbarManager = new ToolbarManager();
    render() {
        return (
            <div id={this.props.id} className={this.props.className}>
                <div id={this.getContainerId()} className="editorContainer canvasEditMode">
                    <div className="toolbar">
                        <Toolbar manager={this.toolbarManager} />
                    </div>
                    <div className="editorCanvasContainer">
                        <canvas id={this.getEditorId()} />
                    </div>
                </div>
                <div className="editorSideMenu">
                    <SideMenu getState={this.getCurrentState} loadState={this.loadState} />
                </div>
            </div>
        );
    }

    componentDidMount() {
        const container = document.getElementById(this.getContainerId());
        if (!(container instanceof HTMLDivElement)) {
            return;
        }
        window.onwheel = () => false;
        this.petriEditor = new Petri(
            this.getEditorId(),
            container,
            this.toolbarManager,
        );
        if (typeof networkJson === 'string') {
            const ser = JSON.parse(networkJson);
            this.petriEditor.deserialize(ser);
        }
    }

    private getCurrentState = () => {
        return this.petriEditor.serialize();
    }

    private loadState = (ser: SerializedPetri) => {
        this.petriEditor.deserialize(ser);
    }

    private getEditorId = () => this.props.id + 'petriEditor';
    private getContainerId = () => this.props.id + 'petriEditorContainer';
}
