import * as React from 'react';
import { Tab, Tabs, TabPanel, TabList } from 'react-tabs';
import { SerializedPetri } from './Petri';
import { Reachability } from './SideMenu/Reachability';
import { Structural } from './SideMenu/Structural';
import { Help } from './SideMenu/Help';
import { SaveLoad } from './SideMenu/SaveLoad';

interface SideMenuProps {
    getState: () => SerializedPetri;
    loadState: (s: SerializedPetri) => void;
}

export class SideMenu extends React.Component<SideMenuProps, null> {
    render() {
        return (
            <Tabs>
                <TabList>
                    <Tab>Help</Tab>
                    <Tab>Reachability</Tab>
                    <Tab>Structural</Tab>
                    <Tab>Save / Load</Tab>
                </TabList>
                <TabPanel>
                    <Help />
                </TabPanel>
                <TabPanel>
                    <Reachability getState={this.props.getState} />
                </TabPanel>
                <TabPanel>
                    <Structural getState={this.props.getState} />
                </TabPanel>
                <TabPanel>
                    <SaveLoad getState={this.props.getState} loadState={this.props.loadState} />
                </TabPanel>
            </Tabs>
        );
    }
}