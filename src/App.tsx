import * as React from 'react';
import './App.css';
import './react-tabs.css';
import { Editor } from './PetriEditor/Editor';

class App extends React.Component<{}, null> {
  render() {
    return (
      <div className="App">
        <Editor id="petriEditor" className="petriEditor" />
      </div>
    );
  }
}
document.title = 'Petri Network Modeler';
export default App;
