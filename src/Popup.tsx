import * as React from 'react';

export class Popup extends React.Component<{ isOpen: boolean }, void> {
    render() {
        return (
            <div className={'popup-overlay' + (this.props.isOpen ? '' : ' closed')}>
                <div className="popup-content">
                    {this.props.children}
                </div>
            </div>
        );
    }
}