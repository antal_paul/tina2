﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetriNetComparers.Models;

namespace PetriNetComparers
{
    public class PetriNetComparer
    {
        public static CompareResult Compare(PetriNetwork net1, PetriNetwork net2)
        {
            if (net1.Transitions.Count != net2.Transitions.Count)
            {
                return new CompareResult(false, $"Different number of transitions: \n" +
                                                $"{net1.Name}: {net1.Transitions.Count}\n" +
                                                $"{net2.Name}: {net2.Transitions.Count}", null);
            }
            if (net1.Locations.Count == net2.Locations.Count)
            {
                return TestNetworkIsomorphism(net1, net2);
            }
            return TestReachabilityIsomorphism(net1, net2);
        }

        public static CompareResult Compare(string net1Json, string net1Name, string net2Json, string net2Name)
        {
            return Compare(getFromJsonString(net1Json, net1Name), getFromJsonString(net2Json, net2Name));
        }

        public static CompareResult Compare(SerializedPetri ser1, string ser1Name, SerializedPetri ser2, string ser2Name)
        {
            return Compare(getFromSerializedNet(ser1, ser1Name), getFromSerializedNet(ser2, ser2Name));
        }

        private static CompareResult TestNetworkIsomorphism(PetriNetwork net1, PetriNetwork net2)
        {
            var g1 = Graph.FromPetriNet(net1);
            var g2 = Graph.FromPetriNet(net2);
            var result = GraphIsomorphismCalculator.GetMapping(g1, g2);
            if (!result.IsSuccessful)
            {
                return TestReachabilityIsomorphism(net1, net2);
            }
            
            Func<Vertex, Vertex> mapFunc = (node) =>
            {
                var index = g1.Nodes.IndexOf(node);
                return g2.Nodes[result.Mapping[index]];
            };
            if (g1.Nodes.Any(
                node => node.Children.Any(child => child.Value != mapFunc(node).Children[mapFunc(child.Key)])))
            {
                return TestReachabilityIsomorphism(net1, net2);
            }

            var message = "Found total match:\n";
            for (var i = 0; i < result.Mapping.Length; i++)
            {
                message += g1.Nodes[i].Type == "T"
                    ? net1.Transitions[g1.Nodes[i].Id].Name
                    : net1.Locations[g1.Nodes[i].Id].Name;

                message += " --> ";

                message += g2.Nodes[result.Mapping[i]].Type == "T"
                    ? net2.Transitions[g2.Nodes[result.Mapping[i]].Id].Name
                    : net2.Locations[g2.Nodes[result.Mapping[i]].Id].Name;
                message += "\n";
            }
            return new CompareResult(true, message, result);

        }
        private static CompareResult TestReachabilityIsomorphism(PetriNetwork net1, PetriNetwork net2, 
            GraphIsomorphismResult graphIsomorphResult = null)
        {
            var result = TreeIsomorphismCalculator.CheckTransitionIsomorphism(net1, net2);
            if (result != null)
            {
                var message = "Found transition match: \n" + 
                    string.Join("\n", result.Select(p => $"{ p.Item1.Name} -> {p.Item2.Name}"));
                return new CompareResult(true, message, graphIsomorphResult);
            }
            return new CompareResult(false, "", graphIsomorphResult);
        }

        private static PetriNetwork getFromJsonString(string jsonString, string name)
        {
            return new PetriNetwork(SerializedPetri.GetFromJson(jsonString), name);
        }

        private static PetriNetwork getFromSerializedNet(SerializedPetri ser, string name)
        {
            return new PetriNetwork(ser, name);
        }

    }
}
