﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace PetriNetComparers.Models
{
    public class SerializedArc
    {
        [JsonProperty("startId")]
        public string StartId { get; set; }
        [JsonProperty("endId")]
        public string EndId { get; set; }
        [JsonProperty("value")]
        public int Value { get; set; }
    }

    public class SerializedLocation
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("value")]
        public int Value { get; set; }
    }

    public class SerializedTransition
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class SerializedPetri
    {
        [JsonProperty("locs")]
        public SerializedLocation[] Locs { get; set; }
        [JsonProperty("trans")]
        public SerializedTransition[] Trans { get; set; }
        [JsonProperty("arcs")]
        public SerializedArc[] Arcs { get; set; }

        public static SerializedPetri GetFromJson(string json)
        {
            var ser = JsonConvert.DeserializeObject<SerializedPetri>(json);
            return ser;
        }
    }
}
