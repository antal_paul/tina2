using System;
using System.Collections.Generic;
using System.Linq;

namespace PetriNetComparers.Models
{
    public class Marking : Dictionary<string, int?>
    {
        public Marking(Marking marking) : base(marking){}
        public Marking() { }

        public int? Compare(Marking other)
        {
            if (this.Keys.Any(k => !other.ContainsKey(k)))
            {
                throw new Exception("Tried to compare two markings of different length");
            }
            var sign = 0;
            foreach (var key in Keys)
            {
                var compare = CompareMarkValue(this[key], other[key]);
                if (sign == 0 && compare != 0)
                {
                    sign = compare;
                } else if (compare != 0 && sign != compare)
                {
                    return null;
                }
            }
            return sign;
        }

        public static int CompareMarkValue(int? val1, int? val2)
        {
            if (val1 == val2) return 0;
            if (!val1.HasValue) return 1;
            if (!val2.HasValue) return -1;
            return val1 < val2 ? -1 : 1;
        }
    }
}