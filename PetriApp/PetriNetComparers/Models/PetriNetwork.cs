﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetriNetComparers.Models
{
    public class PetriNetwork
    {
        public string Name { get; set; }
        public Dictionary<string, Location> Locations { get; set; } = new Dictionary<string, Location>();
        public Dictionary<string, Transition> Transitions { get; set; } = new Dictionary<string, Transition>();
        public Marking InitialMarking { get; set; } = new Marking();
        public PetriNetwork(SerializedPetri serialized, string name)
        {
            Name = name;
            foreach (var serLoc in serialized.Locs)
            {
                Locations[serLoc.Id] = Location.Deserialize(serLoc);
                InitialMarking[serLoc.Id] = serLoc.Value;
            }
            foreach (var serTran in serialized.Trans)
            {
                Transitions[serTran.Id] = Transition.Deserialize(serTran);
            }

            foreach (var serArc in serialized.Arcs)
            {
                if (Locations.ContainsKey(serArc.StartId)
                    && Transitions.ContainsKey(serArc.EndId))
                {
                    var start = Locations[serArc.StartId];
                    var end = Transitions[serArc.EndId];
                    end.IngoingArcs.Add(new Arc
                    {
                        Target = start,
                        Value = serArc.Value
                    });
                }
                else if(Locations.ContainsKey(serArc.EndId)
                        && Transitions.ContainsKey(serArc.StartId))
                {
                    var start = Transitions[serArc.StartId];
                    var end = Locations[serArc.EndId];
                    start.OutgoingArcs.Add(new Arc
                    {
                        Target = end,
                        Value = serArc.Value
                    });
                }
            }
        }

        public List<Transition> GetSatisfiedTransitions(Marking marking)
        {
            return Transitions.Values.Where(t => t.IsSatisfied(marking)).ToList();
        }

        public Node GetReachabilityTree()
        {
            return ReachabilityTree.BuildTree(this);
        }
    }
}
