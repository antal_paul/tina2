﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetriNetComparers.Models
{
    public class Location
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public static Location Deserialize(SerializedLocation ser)
        {
            return new Location
            {
                Id = ser.Id,
                Name = ser.Name
            };
        }
    }
}
