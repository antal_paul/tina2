﻿using System.Collections.Generic;
using System.Linq;

namespace PetriNetComparers.Models
{
    public class Transition
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<Arc> IngoingArcs { get; set; } = new List<Arc>();
        public List<Arc> OutgoingArcs { get; set; } = new List<Arc>();
        public static Transition Deserialize(SerializedTransition ser)
        {
            return new Transition()
            {
                Id = ser.Id,
                Name = ser.Name
            };
        }

        public bool IsSatisfied(Marking marking) => IngoingArcs.All(a => !marking[a.Target.Id].HasValue
            || marking[a.Target.Id] >= a.Value);

        public Marking Apply(Marking marking)
        {
            var clone = new Marking(marking);
            if (!IsSatisfied(clone))
            {
                return null;
            }
            IngoingArcs.ForEach(a =>
            {
                if (clone[a.Target.Id].HasValue) clone[a.Target.Id] -= a.Value;
            });
            OutgoingArcs.ForEach(a =>
            {
                if (clone[a.Target.Id].HasValue) clone[a.Target.Id] += a.Value;
            });
            return clone;
        }
    }
}
