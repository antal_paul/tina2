﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetriNetComparers.Models
{
    public class Arc
    {
        public Location Target { get; set; }
        public int Value { get; set; }
    }
}
