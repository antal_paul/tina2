﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PetriNetComparers.Models
{
    public class Edge
    {

    }

    public class Node
    {
        public Node()
        {
            Id = new Guid().ToString();
        }

        public readonly string Id;
        public Node Parent { get; set; }
        public Dictionary<string, Node> Children { get; set; } = new Dictionary<string, Node>();
        public Marking Marking { get; set; }

        public void UpdateClass()
        {
            foreach (var child in Children.Values)
            {
                child.UpdateClass();
            }
            this._class = this.Children.Count + ",{" + String.Join(",", Children.Values.OrderBy(c => c.Class)
                              .Select(c => c.Class)) + "}";

        }

        private string _class;
        public string Class => _class;
    }
    public class ReachabilityTree
    {
        public static Node BuildTree(PetriNetwork network)
        {
            var root = new Node()
            {
                Marking = network.InitialMarking
            };
            var work = new Queue<Node>();
            work.Enqueue(root);
            while (work.Count > 0)
            {
                var node = work.Dequeue();
                var marking = node.Marking;
                if (marking == null)
                {
                    break;
                }
                var satisfiedTransitions = network.GetSatisfiedTransitions(marking);
                foreach (var transition in satisfiedTransitions)
                {
                    var nextMarking = transition.Apply(marking);
                    if (nextMarking == null)
                    {
                        throw new Exception("Tried to apply a non satisfied transition");
                    }
                    UpdateIfChainExists(node, nextMarking);
                    var nextNode = new Node()
                    {
                        Marking = nextMarking,
                        Parent = node
                    };
                    node.Children[transition.Id] = nextNode;
                    if (!RepeatsPreviousMarking(node, nextMarking))
                    {
                        work.Enqueue(nextNode);
                    }
                }
            }
            return root;
        }

        private static void UpdateIfChainExists(Node node, Marking marking)
        {
            var currentNode = node;
            while (currentNode != null)
            {
                if (node.Marking.Compare(marking) == -1)
                {
                    var keys = marking.Keys.ToList();
                    foreach (var key in keys)
                    {
                        if (Marking.CompareMarkValue(marking[key], currentNode.Marking[key]) == 1)
                        {
                            marking[key] = null;
                        }
                    }
                    return;
                }
                currentNode = currentNode.Parent;
            }
        }

        private static bool RepeatsPreviousMarking(Node node, Marking marking)
        {
            var currentNode = node;
            while (currentNode != null)
            {
                if (currentNode.Marking.Compare(marking) == 0)
                {
                    return true;
                }
                currentNode = currentNode.Parent;
            }
            return false;
        }
    }
}
