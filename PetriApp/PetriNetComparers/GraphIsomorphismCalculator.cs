﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetriNetComparers.Models;

namespace PetriNetComparers
{
    public class GraphIsomorphismResult
    {
        public int[] Mapping { get; set; }
        public bool IsSuccessful { get; set; }
        public int MaxMappedNodes { get; set; }
    }
    public class Vertex
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public Dictionary<Vertex, int> Children { get; set; } = new Dictionary<Vertex, int>();
    }

    public class Graph
    {
        public List<Vertex> Nodes { get; set; } = new List<Vertex>();

        public static Graph FromPetriNet(PetriNetwork net)
        {
            var graph = new Graph();
            var dict = new Dictionary<string, Vertex>();
            foreach (var loc in net.Locations)
            {
                dict[loc.Key] = new Vertex { Id = loc.Value.Id, Type = "L" + net.InitialMarking[loc.Key] };
            }
            foreach (var trans in net.Transitions)
            {
                dict[trans.Key] = new Vertex { Id = trans.Value.Id, Type = "T" };
                trans.Value.IngoingArcs.ForEach(a =>
                {
                    dict[a.Target.Id].Children[dict[trans.Key]] = a.Value;
                });
                trans.Value.OutgoingArcs.ForEach(a =>
                {
                    dict[trans.Key].Children[dict[a.Target.Id]] = a.Value;
                });
            }
            graph.Nodes = dict.Values.ToList();
            return graph;
        }
    }
    public class GraphIsomorphismCalculator
    {
        public static GraphIsomorphismResult GetMapping(Graph g1, Graph g2)
        {
            var result = new GraphIsomorphismResult()
            {
                IsSuccessful = false,
                MaxMappedNodes = 0
            };

            if (g1.Nodes.Count != g2.Nodes.Count)
            {
                return null;
            }
            var g1Labels = LabelNodes(g1);
            var g2Labels = LabelNodes(g2);

            var t = 0;
            var mapping = new int[g1.Nodes.Count];
            for (var i = 0; i < mapping.Length; i++)
            {
                mapping[i] = -1;
            }
            while (true)
            {
                if (t >= g1.Nodes.Count)
                {
                    result.IsSuccessful = true;
                    result.Mapping = mapping.ToArray();
                    result.MaxMappedNodes = t;
                    break;
                }
                if (t < 0)
                {
                    break;
                }
                var next = g2Labels.Select((l, i) => new {label = l, index = i})
                    .FirstOrDefault(k =>
                        !mapping.Contains(k.index)
                        && k.label == g1Labels[t]
                        && k.index > mapping[t]);
                if (next == null)
                {
                    if (t > result.MaxMappedNodes)
                    {
                        result.MaxMappedNodes = t;
                        result.Mapping = mapping.ToArray();
                    }
                    t--;
                    continue;
                }
                mapping[t] = next.index;
                t++;
            }
            return result;
        }

        public static string[] LabelNodes(Graph graph)
        {
            var distanceMatrix = GetDistanceMatrix(graph);
            var rx = GetRX(distanceMatrix);
            var cx = getCX(distanceMatrix);
            var labels = new string[distanceMatrix.Length];
            for (var i = 0; i < distanceMatrix.Length; i++)
            {
                var i1 = i;
                labels[i] = graph.Nodes[i1].Type + "|" + string.Join("|", rx[i].Select((r, j) => r + "~" + cx[i1][j]));
            }
            return labels;
        }

        public static int[][] GetDistanceMatrix(Graph graph)
        {
            var dist = new int[graph.Nodes.Count][];
            for (var i = 0; i < graph.Nodes.Count; i++)
            {
                dist[i] = new int[graph.Nodes.Count];
                for (var j = 0; j < graph.Nodes.Count; j++)
                {
                    dist[i][j] = Int32.MaxValue / 2;
                }
                dist[i][i] = 0;
            }
            foreach (var node in graph.Nodes)
            {
                foreach (var child in node.Children.Keys)
                {
                    dist[graph.Nodes.IndexOf(node)][graph.Nodes.IndexOf(child)] = 1;
                }
            }
            for (var k = 0; k < graph.Nodes.Count; k++)
            {
                for (var i = 0; i < graph.Nodes.Count; i++)
                {
                    for (var j = 0; j < graph.Nodes.Count; j++)
                    {
                        dist[i][j] = Math.Min(dist[i][j], dist[i][k] + dist[k][j]);
                    }
                }
            }
            return dist;
        }

        public static int[][] GetRX(int[][] distanceMatrix)
        {
            var size = distanceMatrix.Length;
            var result = new int[size][];
            for (var i = 0; i < size; i++)
            {
                result[i] = new int[size];
                for (var j = 0; j < size; j++)
                {
                    if(distanceMatrix[i][j] < result[0].Length)
                        result[i][distanceMatrix[i][j]]++;
                }
            }
            return result;
        }

        public static int[][] getCX(int[][] distanceMatrix)
        {
            var size = distanceMatrix.Length;
            var result = new int[size][];
            for (var i = 0; i < size; i++)
            {
                result[i] = new int[size];
                for (var j = 0; j < size; j++)
                {
                    if (distanceMatrix[j][i] < result[0].Length)
                        result[i][distanceMatrix[j][i]]++;
                }
            }
            return result;
        }
    }
}
