﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetriNetComparers
{
    public class CompareResult
    {
        public readonly bool AreEqual;
        public readonly string Message;
        public readonly GraphIsomorphismResult IsomorphismResult;
        public CompareResult(bool areEqual, string message, GraphIsomorphismResult isomorphismResult)
        {
            AreEqual = areEqual;
            Message = message;
            IsomorphismResult = isomorphismResult;
        }
    }
}
