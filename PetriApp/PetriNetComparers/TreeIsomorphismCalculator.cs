﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetriNetComparers.Models;

namespace PetriNetComparers
{
    public class Usages : Dictionary<string, List<Tuple<Node, Node>>> { }
    public class TreeIsomorphismCalculator
    {
        public static List<Tuple<Transition, Transition>> CheckTransitionIsomorphism(PetriNetwork net1, PetriNetwork net2)
        {
            if (net1.Transitions.Count != net2.Transitions.Count)
            {
                return null;
            }
            var root1 = ReachabilityTree.BuildTree(net1);
            var root2 = ReachabilityTree.BuildTree(net2);
            root1.UpdateClass();
            root2.UpdateClass();
            
            if (root1.Class != root2.Class)
                return null;
            var usages1 = GetTransitionUsages(root1);
            var usages2 = GetTransitionUsages(root2);
            var t = 0;
            var mapping = new int[net1.Transitions.Count];
            for (var i = 0; i < mapping.Length; i++)
            {
                mapping[i] = -1;
            }
            while (true)
            {
                if (t >= net1.Transitions.Count)
                {
                    return mapping.Select((m, i) => new Tuple<Transition, Transition>(
                        net1.Transitions.ElementAt(i).Value,
                        net2.Transitions.ElementAt(m).Value
                    )).ToList();
                }
                if (t < 0)
                {
                    return null;
                }
                var currentTransition = net1.Transitions.ElementAt(t).Value;
                var next = net2.Transitions.Select((trans, i) => new {Value = trans.Value, Index = i})
                    .FirstOrDefault(trans => !mapping.Contains(trans.Index) && IsMatch(currentTransition, trans.Value, usages1, usages2));
                if (next == null)
                {
                    t--;
                    continue;
                }
                mapping[t] = next.Index;
                t++;
            }
        }

        public static bool IsMatch(Transition t1, Transition t2, Usages usage1, Usages usage2)
        {
            if (usage1[t1.Id].Count != usage2[t2.Id].Count)
                return false;
            var list1 = usage1[t1.Id].ToList();
            var list2 = usage2[t2.Id].ToList();
            foreach (var pair in list1)
            {
                var correspondent = list2.FirstOrDefault(p => p.Item1.Class == pair.Item1.Class && p.Item2.Class == pair.Item2.Class);
                if (correspondent == null)
                    return false;
                list2.Remove(correspondent);
            }
            return list2.Count == 0;
        }

        public static Usages GetTransitionUsages(Node root)
        {
            var result = new Usages();
            var work = new Queue<Tuple<Node,int>>();
            work.Enqueue(new Tuple<Node, int>(root,0));

            while (work.Any())
            {
                var current = work.Dequeue();
                var currentNode = current.Item1;
                var currentLevel = current.Item2;
                foreach (var child in currentNode.Children)
                {
                    if (!result.ContainsKey(child.Key))
                    {
                        result[child.Key] = new List<Tuple<Node, Node>>();
                    }
                    result[child.Key].Add(new Tuple<Node, Node>(currentNode, child.Value));
                    work.Enqueue(new Tuple<Node, int>(child.Value, currentLevel + 1));
                }
            }

            return result;
        }
    }
}
