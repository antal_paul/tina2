﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Database.Entities;

namespace PetriApp.Models
{
    public class PetriListItem
    {
        public int Id { get; set; }
        public string NetworkName { get; set; }
        public DateTime TimeCreated { get; set; }

        public static PetriListItem FromDto(PNetDto pn)
        {
            return new PetriListItem()
            {
                Id = pn.Id,
                NetworkName = pn.NetworkName,
                TimeCreated = pn.TimeCreated
            };
        }
    }
}