﻿using System.ComponentModel;

namespace PetriApp.Models
{
    public class UserViewModel
    {
        public string Id { get; set; }
        [DisplayName("Prenume")]
        public string FirstName { get; set; }
        [DisplayName("Nume")]
        public string LastName { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        public PetriListItem[] Networks { get; set; }
        [DisplayName("Grupa")]
        public string Group { get; set; }
    }
}