﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PetriApp.Models
{
    public class UserListViewModel
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public static UserListViewModel FromApplicationUser(ApplicationUser user)
        {
            return new UserListViewModel()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email
            };
        }
    }
}