﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PetriApp.Startup))]
namespace PetriApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
