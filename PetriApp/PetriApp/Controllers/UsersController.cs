﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Database;
using PetriApp.Models;

namespace PetriApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private PetriRepository _repo = new PetriRepository();
        // GET: Users
        public ActionResult Index()
        {
            var users = ApplicationDbContext.Create().Users
                .Select(UserListViewModel.FromApplicationUser)
                .ToList();

            return View(users);
        }

        public ActionResult Details(string id)
        {
            var user = ApplicationDbContext.Create().Users.SingleOrDefault(u => u.Id == id);
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "User does not exist");
            }
            var networks = _repo.GetByUser(user.UserName)
                .Select(PetriListItem.FromDto)
                .ToArray();
            var viewModel = new UserViewModel()
            {
                Id = id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Networks = networks,
                Group = user.Group,
            };
            return View(viewModel);
        }

        public ActionResult Delete(string id)
        {
            var user = ApplicationDbContext.Create().Users.SingleOrDefault(u => u.Id == id);
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "User does not exist");
            }
            var viewModel = new UserViewModel()
            {
                Id = id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
            };
            return View(viewModel);
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            var context = ApplicationDbContext.Create();
            var user = context.Users.SingleOrDefault(u => u.Id == id);
            if (user == null)
            {
                return RedirectToAction("Index");
            }
            context.Users.Remove(user);
            context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}