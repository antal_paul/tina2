﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Database;
using Database.Entities;
using PetriApp.Models;
using PetriNetComparers;
using PetriNetComparers.Models;

namespace PetriApp.Controllers
{
    [Authorize]
    public class PetriController : Controller
    {
        private PetriRepository repo = new PetriRepository();

        // GET: Petri
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult NetList()
        {
            var user = User.Identity.Name;
            var networks = repo.GetByUser(user).Select(PetriListItem.FromDto);
            return View(networks);
        }

        [HttpPost]
        [ActionName("Create")]
        public JsonResult Save(string json, string name)
        {
            var user = User.Identity.Name;
            var network = repo.GetByName(user, name);
            if (network != null)
                return Json(new { status = HttpStatusCode.BadRequest, message = "Name already exists" });
            network = new PNetDto()
            {
                JsonNetwork = json,
                NetworkName = name,
                Username = user,
                LastModified = DateTime.Now,
                TimeCreated = DateTime.Now
            };
            repo.Create(network);
            repo.Save();
            var id = repo.GetByName(user, name).Id;
            return Json(new { status = HttpStatusCode.OK, message = "Save succesful", id });
        }

        public ActionResult Edit(int id)
        {
            var net = repo.GetById(id);
            var user = User.Identity.Name;
            if (!User.IsInRole("Admin") && net.Username != user)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized,
                    "You cannot edit this network because it doesn't belong to you");
            ViewBag.Id = net.Id;
            ViewBag.NetworkJson = net.JsonNetwork;
            ViewBag.NetworkName = net.NetworkName;
            return View("Create");
        }

        [HttpPost]
        public JsonResult Edit(int id, string name, string json)
        {
            var net = repo.GetById(id);
            if (net == null)
                return Json(new { status = HttpStatusCode.BadRequest, message = "Network does not exist" });
            var user = User.Identity.Name;
            if (net.Username != user)
                return Json(new { status = HttpStatusCode.Unauthorized, message = "You cannot edit this network because it doesn't belong to you" });
            net.JsonNetwork = json;
            net.NetworkName = name;
            net.LastModified = DateTime.Now;
            repo.Update(net);
            repo.Save();
            var ser = SerializedPetri.GetFromJson(json);
            return Json(new { status = HttpStatusCode.OK, message = "Update succesful" });
        }

        public ActionResult Delete(int id)
        {
            var net = repo.GetById(id);
            var user = User.Identity.Name;
            if (net.Username != user)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized,
                    "You cannot delete this network because it doesn't belong to you");
            return View(PetriListItem.FromDto(net));
        }

        [HttpPost]
        [ActionName("Delete")]
        public RedirectToRouteResult DeleteConfirm(int id)
        {
            var result = RedirectToAction("NetList");
            var net = repo.GetById(id);
            if (net == null)
                return result;
            var user = User.Identity.Name;
            if (net.Username != user)
                return result;
            repo.Remove(id);
            repo.Save();
            return result;
        }

        [HttpGet]
        public ActionResult CompareSelection()
        {
            var user = User.Identity.Name;
            var networks = repo.GetByUser(user).Select(PetriListItem.FromDto);
            return View(networks);
        }

        [HttpPost]
        public ActionResult Compare(int firstNet, int secondNet)
        {
            var net1 = repo.GetById(firstNet);
            var net2 = repo.GetById(secondNet);
            ViewBag.FirstNet = net1;
            ViewBag.SecondNet = net2;
            var result = PetriNetComparer.Compare(net1.JsonNetwork, net1.NetworkName, net2.JsonNetwork, net2.NetworkName);
            return View("CompareResults", result);
        }
    }
}