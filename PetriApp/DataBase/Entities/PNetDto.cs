﻿using System;

namespace Database.Entities
{
    public class PNetDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string NetworkName { get; set; }
        public string JsonNetwork { get; set; }
        public DateTime TimeCreated { get; set; }
        public DateTime LastModified { get; set; }
    }
}
