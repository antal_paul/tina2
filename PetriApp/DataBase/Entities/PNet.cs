﻿using System;

namespace Database.Entities
{
    class PNet
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string NetworkName { get; set; }
        public string JsonNetwork { get; set; }

        public DateTime TimeCreated { get; set; }
        public DateTime LastModified { get; set; }
        public void UpdateFromDto(PNetDto net)
        {
            JsonNetwork = net.JsonNetwork;
            NetworkName = net.NetworkName;
            Username = net.Username;
            TimeCreated = net.TimeCreated;
            LastModified = net.LastModified;
        }
        public static PNet FromDto(PNetDto net)
        {
            return new PNet()
            {
                Id = net.Id,
                JsonNetwork = net.JsonNetwork,
                NetworkName = net.NetworkName,
                Username = net.Username,
                TimeCreated = net.TimeCreated,
                LastModified = net.LastModified
            };

        }
        public static PNetDto ToDto(PNet net)
        {
            return new PNetDto()
            {
                Id = net.Id,
                JsonNetwork = net.JsonNetwork,
                NetworkName = net.NetworkName,
                Username = net.Username,
                TimeCreated = net.TimeCreated,
                LastModified = net.LastModified
            };
        }
    }
}
