﻿using System.Data.Entity;
using Database.Entities;

namespace Database
{
    class PetriContext : DbContext
    {
        public DbSet<PNet> PNets { get; set; }
        private PetriContext() { }
        private static PetriContext _instance;
        public static PetriContext Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new PetriContext();
                }
                return _instance;
            }
        }
    }
}
