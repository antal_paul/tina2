﻿using System;

namespace Database
{
    class DataBase
    {
        public static void Main()
        {
            var repo = new PetriRepository();
            repo.Create(new Entities.PNetDto() { Username = "Paul", NetworkName = "Test", LastModified= DateTime.Now, TimeCreated= DateTime.Now });
            repo.Save(); 
        }
    }
}
