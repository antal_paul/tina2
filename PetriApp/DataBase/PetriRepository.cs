﻿using System.Linq;
using Database.Entities;

namespace Database
{
    public class PetriRepository
    {
        public PNetDto GetById(int id)
        {
            return PetriContext.Instance.PNets
                .Select(PNet.ToDto)
                .SingleOrDefault(pn => pn.Id == id);
        }

        public PNetDto[] GetAll()
        {
            return PetriContext.Instance.PNets
                .Select(PNet.ToDto)
                .ToArray();
        }
        public PNetDto GetByName(string username, string netName)
        {
            return PetriContext.Instance.PNets
                .Select(PNet.ToDto)
                .SingleOrDefault(pn => pn.Username == username && pn.NetworkName == netName);
        }

        public PNetDto[] GetByUser(string username)
        {
            return PetriContext.Instance.PNets
                .Select(PNet.ToDto)
                .Where(pn => pn.Username == username)
                .ToArray();
        }

        public void Create(PNetDto newNet)
        {
            var net = PNet.FromDto(newNet);
            PetriContext.Instance.PNets.Add(net);
        }

        public void Update(PNetDto updatedNet)
        {
            var net = PetriContext.Instance.PNets.SingleOrDefault(pn => pn.Id == updatedNet.Id);
            if (net == null) return;
            net.UpdateFromDto(updatedNet);
        }

        public void Remove(int Id)
        {
            var net = PetriContext.Instance.PNets.SingleOrDefault(pn => pn.Id == Id);
            if (net == null) return;
            PetriContext.Instance.PNets.Remove(net);
        }

        public void Save()
        {
            PetriContext.Instance.SaveChanges();
        }
    }
}
