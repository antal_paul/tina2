﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetriNetComparers;
using PetriNetComparers.Models;

namespace PNCTestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var jsonNet1 =
                "{\"locs\":[{\"x\":531,\"y\":140,\"name\":\"p0\",\"value\":1,\"id\":\"160f7347-ba1f-4c72-bcf0-0e6b4fd514f8\"},{\"x\":528,\"y\":459,\"name\":\"p1\",\"value\":0,\"id\":\"2c1d2b66-3482-41df-8ec8-0039107ea192\"}],\"trans\":[{\"x\":408,\"y\":279,\"name\":\"t0\",\"id\":\"3f4a3432-7954-4fb1-86f3-cc0c221e251b\"},{\"x\":641,\"y\":300,\"name\":\"t1\",\"id\":\"455c96bf-ed47-44be-8fc4-c7775b3c6eb6\"}],\"arcs\":[{\"startId\":\"160f7347-ba1f-4c72-bcf0-0e6b4fd514f8\",\"endId\":\"455c96bf-ed47-44be-8fc4-c7775b3c6eb6\",\"intermediaryPoints\":[],\"value\":1},{\"startId\":\"455c96bf-ed47-44be-8fc4-c7775b3c6eb6\",\"endId\":\"2c1d2b66-3482-41df-8ec8-0039107ea192\",\"intermediaryPoints\":[],\"value\":1},{\"startId\":\"2c1d2b66-3482-41df-8ec8-0039107ea192\",\"endId\":\"3f4a3432-7954-4fb1-86f3-cc0c221e251b\",\"intermediaryPoints\":[],\"value\":1},{\"startId\":\"3f4a3432-7954-4fb1-86f3-cc0c221e251b\",\"endId\":\"160f7347-ba1f-4c72-bcf0-0e6b4fd514f8\",\"intermediaryPoints\":[],\"value\":1}]}";
            var jsonNet2 =
                "{\"locs\":[{\"x\":531,\"y\":140,\"name\":\"p0\",\"value\":1,\"id\":\"160f7347-ba1f-4c72-bcf0-0e6b4fd514f8\"},{\"x\":528,\"y\":459,\"name\":\"p1\",\"value\":0,\"id\":\"2c1d2b66-3482-41df-8ec8-0039107ea192\"}],\"trans\":[{\"x\":408,\"y\":279,\"name\":\"t0\",\"id\":\"3f4a3432-7954-4fb1-86f3-cc0c221e251b\"},{\"x\":641,\"y\":300,\"name\":\"t1\",\"id\":\"455c96bf-ed47-44be-8fc4-c7775b3c6eb6\"}],\"arcs\":[{\"startId\":\"160f7347-ba1f-4c72-bcf0-0e6b4fd514f8\",\"endId\":\"455c96bf-ed47-44be-8fc4-c7775b3c6eb6\",\"intermediaryPoints\":[],\"value\":1},{\"startId\":\"455c96bf-ed47-44be-8fc4-c7775b3c6eb6\",\"endId\":\"2c1d2b66-3482-41df-8ec8-0039107ea192\",\"intermediaryPoints\":[],\"value\":1},{\"startId\":\"2c1d2b66-3482-41df-8ec8-0039107ea192\",\"endId\":\"3f4a3432-7954-4fb1-86f3-cc0c221e251b\",\"intermediaryPoints\":[],\"value\":1},{\"startId\":\"3f4a3432-7954-4fb1-86f3-cc0c221e251b\",\"endId\":\"160f7347-ba1f-4c72-bcf0-0e6b4fd514f8\",\"intermediaryPoints\":[],\"value\":1}]}";


            var result = PetriNetComparer.Compare(jsonNet1,"net1", jsonNet2, "net2");
            Console.Write(result.AreEqual ? result.Message : "different" );
            var a = 2;
        }

    }
}
